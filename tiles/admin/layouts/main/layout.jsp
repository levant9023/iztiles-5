<%--
  User: efanchik
  Date: 8/2/15
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<c:set var="req" value="${pageContext.request}" scope="request"/>
<c:set var="baseURL" value="${req.scheme}://${req.serverName}:${req.serverPort}${req.contextPath}" scope="request"/>
<c:set var="lang" value="${empty requestScope.locale ? 'en' : fn:substring(requestScope.locale,0, 2)}" scope="request"/>

<html lang="<c:out value="${lang}"/>">
  <head>
    <base href="${baseURL}/admin/">
    <tiles:insertAttribute name="head"/>
  </head>
  <body>
      <tiles:insertAttribute name="content"/>
      <div class="clearfix"></div>
  </body>
</html>

