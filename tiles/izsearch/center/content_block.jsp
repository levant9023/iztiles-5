<%--
  User: eFanchik
  Date: 07.07.2014
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<link type="text/css" rel="stylesheet" href="${baseURL}/resources/css/izsearch/filters/main-menu/filters.css" />

<div id="sideCenter" class="center_container">
  <div id="search-text" class="content">
    <tiles:insertAttribute name="sections"/>
    <tiles:insertAttribute name="paginator"/>
  </div>
  <div id="search-images" class="content">
    <tiles:insertAttribute name="right_block"/>
  </div>
</div>
