<%--  
  User: eFanchik
  Date: 07.07.2014
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<link type="text/css" rel="stylesheet" href="${baseURL}/resources/css/izsearch/filters/filters.css" />


<c:set var="path" value="${requestScope.wpath}" />
<c:set var="cl" value="" />
<c:if test="${not empty path}">
  <c:choose>
    <c:when test="${fn:contains(path, 'images.html')}">
      <c:set var="cl" value="filter-images" />
      <c:set var="nbf" value="false"/>
    </c:when>
    <c:when test="${fn:contains(path, 'videos.html')}">
      <c:set var="cl" value="filter-videos" />
      <c:set var="nbf" value="false"/>
    </c:when>
    <c:otherwise>
      <c:set var="cl" value="filter-text" />
      <c:set var="nbf" value="true"/>
    </c:otherwise>
  </c:choose>
</c:if>

<div id="sideCenter" class="center_container">
  <div id="search-text" class="content <c:out value="${cl}" default=""/>">
    <tiles:insertAttribute name="hcancarousel"/>
    <tiles:insertAttribute name="sections"/>
    <tiles:insertAttribute name="paginator"/>
  </div>
  <div id="search-images" class="content">
    <tiles:insertAttribute name="right_block"/>
   </div>
</div>