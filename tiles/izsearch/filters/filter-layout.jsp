<%--
  User: eFanchik
  Date: 07.07.2014
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="iz" uri="/WEB-INF/tlds/izsearch.tld" %>


<!DOCTYPE HTML>
<c:set var="query" value="${empty requestScope.q ? '' : requestScope.q}" scope="request"/>
<c:set var="lang" value="${empty requestScope.locale ? 'en' : fn:substring(requestScope.locale,0, 2)}" scope="request"/>
<c:set var="req" value="${pageContext.request}" scope="request"/>
<c:set var="baseURL" value="${req.scheme}://${req.serverName}:${req.serverPort}${req.contextPath}" scope="request"/>
<c:set var="meta_title" value="${requestScope.meta_title}" scope="request"/>

<html lang="<c:out value="${lang}"/>">
<head>
  <base href="${baseURL}/">
  <meta http-equiv="X-UA-Compatible" content="IE=edge; charset=utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description"
        content="izSearch search engine, the new private way to search the web. Search engine that finds and returns relevant web sites, images, videos and realtime results.">
  <meta name="keywords"
        content="search engine, web search, privacy, private search, image search, video search, search engine privacy, search engine optimization, search engine marketing, easy search">
  <title>iZSearch search engine | <c:out value="${meta_title}" default="iZSearch search engine. Desktop version."/> </title>
  <link rel="shortcut icon" type="image/png" href="${baseURL}/resources/img/favicon.png"/>
  <link rel="search" type="application/opensearchdescription+xml" title="iZSearch" href="${baseURL}/provider.xml">
  <link rel="stylesheet" type="text/css" href="${baseURL}/resources/js/production/bootstrap/css/bootstrap.css"/>
  <link rel="stylesheet" type="text/css" href="${baseURL}/resources/font-awesome-4.3.0/css/font-awesome.min.css"/>
  <link rel="stylesheet" type="text/css" href="${baseURL}/resources/js/production/bootstrap/css/bootstrap-datetimepicker.min.css"/>
  <link rel="stylesheet" type="text/css" href="${baseURL}/resources/js/production/bootstrap/css/jquery-ui.min.css"/>
  <link rel="stylesheet" type="text/css" href="${baseURL}/resources/css/izsearch/custom.css"/>
  <%--<link rel="stylesheet" type="text/css" href="${baseURL}/resources/css/izsearch/media.css"/>--%>
  <link rel="stylesheet" type="text/css" href="${baseURL}/resources/js/production/plugins/owl-carousel/owl.carousel.css"/>
  <link rel="stylesheet" type="text/css" href="${baseURL}/resources/js/production/css/owl.theme.css"/>

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->

  <script type="text/javascript" src="${baseURL}/resources/js/production/bootstrap/js/jquery-1.11.1.min.js"></script>
  <script type="text/javascript" src="${baseURL}/resources/js/production/bootstrap/js/moment-locales.js"></script>
  <script type="text/javascript" src="${baseURL}/resources/js/production/bootstrap/js/bootstrap.js"></script>
  <script type="text/javascript" src="${baseURL}/resources/js/production/bootstrap/js/bootstrap-datetimepicker.min.js"></script>
  <script type="text/javascript" src="${baseURL}/resources/js/production/bootstrap/js/can.min.js"></script>
  <script type="text/javascript" src="${baseURL}/resources/js/production/plugins/owl-carousel/owl.carousel.js"></script>
  <script type="text/javascript" src="${baseURL}/resources/js/production/modules/js.cookie.js"></script>
  <script type="text/javascript" src="${baseURL}/resources/js/production/mvc/classes.js"></script>

  <script type="text/javascript">
    var query = "<c:out value='${query}' default=''/>";
    var lang = "<c:out value='${lang}' default='en'/>";
    var geo = {};
  </script>
  <script type="text/javascript" async
          data-main="${baseURL}/resources/js/production/mvc/main_filter.js"
          src="${baseURL}/resources/js/production/require.js">
  </script>

  <%--<script type="text/javascript">
    var request = new XMLHttpRequest();
    request.open("GET", "/ads/geo.html"+window.location.search, false);
    request.send(null);
    try {
      if (request.readyState === 4 && request.status === 200) {
        geo = JSON.parse(request.responseText);
      }
    }catch (e){
      console.log(e);
    }
  </script>--%>
</head>

<body>

<c:choose>
  <c:when test="${iz:basename(requestScope['javax.servlet.forward.request_uri']) eq 'images'}" >
    <c:set var="middle_style" value='style="margin-top:0px"' scope="request" />
    <c:set var="header_style" value='style="min-height:70px"' scope="request" />
  </c:when>
  <c:when test="${iz:basename(requestScope['javax.servlet.forward.request_uri']) eq 'video'}" >
    <c:set var="middle_style" value='style="margin-top:0px"' scope="request" />
    <c:set var="header_style" value='style="min-height:70px"' scope="request" />
  </c:when>
  <c:otherwise>
    <c:set var="middle_style" value="" scope="request" />
    <c:set var="header_style" value="" scope="request" />
  </c:otherwise>
</c:choose>

<!-- wrapper -->
<div id="wrapper" class="container-fluid">
  <tiles:insertAttribute name="header"/>
  <section id="middle" class="row" ${middle_style} >
    <tiles:insertAttribute name="leftblock"/>
    <tiles:insertAttribute name="centerblock"/>
    <tiles:insertAttribute name="rightblock"/>
    <tiles:insertAttribute name="rightads"/>
  </section>
</div>

<!-- Google Analytics counter -->
<script>
  (function (i, s, o, g, r, a, m) {
    i['GoogleAnalyticsObject'] = r;
    i[r] = i[r] || function () {
          (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
    a = s.createElement(o),
        m = s.getElementsByTagName(o)[0];
    a.async = 1;
    a.src = g;
    m.parentNode.insertBefore(a, m)
  })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
  ga('create', 'UA-58665066-1', 'auto');
  ga('require', 'linkid', 'linkid.js');
  ga('send', 'pageview');
  setTimeout("ga('send', 'event', 'Pageview', 'Page visit 15 sec. or more')", 1500);
</script>

<%--
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
  (function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
      try {
        w.yaCounter31760636 = new Ya.Metrika({
          id:31760636,
          clickmap:true,
          trackLinks:true,
          accurateTrackBounce:true,
          webvisor:true,
          trackHash:true
        });
      } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = "https://mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
      d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
  })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/31760636" style="position:absolute; left:-9999px;" alt="" /></div></noscript>--%>

<tiles:insertAttribute name="footer"/>
<h1 class="grob">${meta_title}</h1>
<h2 class="grob">${meta_title}</h2>
</body>
</html>