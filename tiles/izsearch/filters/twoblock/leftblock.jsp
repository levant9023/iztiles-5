<%--
  Created by IntelliJ IDEA.
  User: efanchik
  Date: 3/3/16
  Time: 10:47 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="/WEB-INF/tlds/izsearch.tld" prefix="iz" %>

<c:set var="bas" value="${cookie['_bas'].value}" scope="page"/>
<c:set var='visible' value='' />

<c:choose>
  <c:when test="${bas eq 1}">
    <c:set var='visible' value='style="display: block"' />
  </c:when>
  <c:otherwise>
    <c:set var='visible' value='style="display: hidden"' />
  </c:otherwise>
</c:choose>
