<%--
  Document: header
  User:     eFanchik
  Date:     07.07.14
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="iz" uri="/WEB-INF/tlds/izsearch.tld" %>

<c:set var="bas" value="${cookie['_bas'].value}" scope="page"/>
<c:set var='visible' value='' />

<c:choose>
  <c:when test="${bas eq 1}">
    <c:set var='visible' value='style="display: block"' />
  </c:when>
  <c:otherwise>
    <c:set var='visible' value='style="display: hidden"' />
  </c:otherwise>
</c:choose>

<c:set var="count" value="10" scope="page"/>

<c:url var="main_url" value="${baseURL}">
  <c:if test="${param.q}">
    <c:param name="q" value="${param.q}"/>
  </c:if>
</c:url>

<c:url var="images_url" value="images.html">
  <c:if test="${param.q}">
    <c:param name="q" value="${param.q}"/>
  </c:if>
</c:url>

<c:url var="faves_url" value="buttons.html">
  <c:if test="${param.q}">
    <c:param name="q" value="${param.q}"/>
  </c:if>
</c:url>

<c:url var="videos_url" value="videos.html">
  <c:if test="${param.q}">
    <c:param name="q" value="${param.q}"/>
  </c:if>
</c:url>

<c:url var="map_url" value="//www.mapquest.com/search/results?centerOnResults=1">
  <c:if test="${param.q}">
    <c:param name="q" value="${param.q}"/>
  </c:if>
</c:url>

<c:url var="news_url" value="news.html">
  <c:if test="${param.q}">
    <c:param name="q" value="${param.q}"/>
  </c:if>
</c:url>

<c:url var="blogs_url" value="blog.html">
  <c:if test="${param.q}">
    <c:param name="q" value="${param.q}"/>
  </c:if>
</c:url>

<c:url var="forums_url" value="forums.html">
  <c:if test="${param.q}">
    <c:param name="q" value="${param.q}"/>
  </c:if>
</c:url>

<c:url var="health_url" value="health.html">
  <c:if test="${param.q}">
    <c:param name="q" value="${param.q}"/>
  </c:if>
</c:url>

<c:url var="food_url" value="food.html">
  <c:if test="${param.q}">
    <c:param name="q" value="${param.q}"/>
  </c:if>
</c:url>

<c:url var="kids_url" value="kids.html">
  <c:if test="${param.q}">
    <c:param name="q" value="${param.q}"/>
  </c:if>
</c:url>

<c:url var="music_url" value="music.html">
  <c:if test="${param.q}">
    <c:param name="q" value="${param.q}"/>
  </c:if>
</c:url>

<c:url var="entertainment_url" value="/entertainment.html">
  <c:if test="${param.q}">
    <c:param name="q" value="${param.q}"/>
  </c:if>
</c:url>

<c:url var="pets_url" value="/pets.html">
  <c:if test="${param.q}">
    <c:param name="q" value="${param.q}"/>
  </c:if>
</c:url>

<c:url var="cars_url" value="/cars.html">
  <c:if test="${param.q}">
    <c:param name="q" value="${param.q}"/>
  </c:if>
</c:url>

<c:url var="science_url" value="/science.html">
  <c:if test="${param.q}">
    <c:param name="q" value="${param.q}"/>
  </c:if>
</c:url>

<c:url var="seniors_url" value="/seniors.html">
  <c:if test="${param.q}">
    <c:param name="q" value="${param.q}"/>
  </c:if>
</c:url>

<c:url var="shopping_url" value="/shopping.html">
  <c:if test="${param.q}">
    <c:param name="q" value="${param.q}"/>
  </c:if>
</c:url>

<c:url var="sports_url" value="/sports.html">
  <c:if test="${param.q}">
    <c:param name="q" value="${param.q}"/>
  </c:if>
</c:url>

<c:url var="tech_url" value="/tech.html">
  <c:if test="${param.q}">
    <c:param name="q" value="${param.q}"/>
  </c:if>
</c:url>

<c:url var="travel_url" value="/travel.html">
  <c:if test="${param.q}">
    <c:param name="q" value="${param.q}"/>
  </c:if>
</c:url>

<c:url var="tv_url" value="/tv.html">
  <c:if test="${param.q}">
    <c:param name="q" value="${param.q}"/>
  </c:if>
</c:url>

<side id="sideLeft" class="left_container">
  <div id="lnav-wrapper">
    <ul id="left-nav-01" class="list-unstyled" style="display: block">
      <li id="all-menu-item">
        <a href="<c:out value='${main_url}'/>">Main</a>
      </li>
      <li id="news-menu-item">
        <span class="img"><img src="${baseURL}/resources/img/nav/news.svg" alt="image"/></span>
        <a href="<c:out value='${news_url}?q=${param.q}'/>">News</a>
        <span class="badge"></span>
      </li>
      <li id="image-menu-item">
        <span class="img"><img src="${baseURL}/resources/img/nav/images.svg" alt="image"/></span>
        <a href="<c:out value='${images_url}?q=${param.q}'/>">Images</a>
        <span class="badge"></span>
      </li>
      <li id="videos-menu-item">
        <span class="img"><img src="${baseURL}/resources/img/nav/videos.svg" alt="image"/></span>
        <a href="<c:out value='${videos_url}?q=${param.q}'/>">Videos</a>
        <span class="badge"></span>
      </li>
      <li id="maps-menu-item">
        <span class="img"><img src="${baseURL}/resources/img/nav/maps.svg" alt="image"/></span>
        <a href="<c:out value='${map_url}?q=${param.q}'/>">Maps</a>
        <span class="badge"></span>
      </li>
      </ul>
      <%--<li id="blogs-menu-item">
        <span class="img"><img src="${baseURL}/resources/img/nav/blogs.svg" alt="image"/></span>
        <a href="<c:out value='${blogs_url}'/>">Blogs</a>
        <span class="badge"></span>
      </li>
      <li id="forums-menu-item">
        <span class="img"><img src="${baseURL}/resources/img/nav/forums.svg" alt="image"/></span>
        <a href="<c:out value='${forums_url}'/>">Forums</a>
        <span class="badge"></span>
      </li>--%>

      <%--
         <div id="lnav-button">
            <span class="img"><img src="${baseURL}/resources/img/nav/more.svg" alt="image"/></span>
            <a href="javascript://">More</a>
            <span class="caret-down"></span>
          </div>
          <ul id="catalog" class="list-unstyled" ${visible}>
            <li id="blogs-menu-item">
              <span class="img"><img src="${baseURL}/resources/img/nav/blogs.svg" alt="image"/></span>
              <a href="<c:out value='${blogs_url}'/>">Blogs</a>
              <span class="badge"></span>
            </li>
            <li id="forums-menu-item">
              <span class="img"><img src="${baseURL}/resources/img/nav/forums.svg" alt="image"/></span>
              <a href="<c:out value='${forums_url}'/>">Forums</a>
              <span class="badge"></span>
            </li>
            <li id="health-menu-item">
              <span class="img"><img src="${baseURL}/resources/img/nav/health.svg" alt="image"/></span>
              <a href="<c:out value='${health_url}'/>">Health</a>
              <span class="badge"></span>
            </li>
            <li id="food-menu-item">
              <span class="img"><img src="${baseURL}/resources/img/nav/food.svg" alt="image"/></span>
              <a href="<c:out value='${food_url}'/>">Food</a>
              <span class="badge"></span>
            </li>
            <li id="music-menu-item">
              <span class="img"><img src="${baseURL}/resources/img/nav/music.svg" alt="image"/></span>
              <a href="<c:out value='${music_url}'/>">Music</a>
              <span class="badge"></span>
            </li>
            <li id="entertainment-menu-item">
              <span class="img"><img src="${baseURL}/resources/img/nav/fun.svg" alt="image"/></span>
              <a href="<c:out value='${entertainment_url}'/>">Fun</a>
              <span class="badge"></span>
            </li>
            <li id="pets-menu-item">
              <span class="img"><img src="${baseURL}/resources/img/nav/pets.svg" alt="image"/></span>
              <a href="<c:out value='${pets_url}'/>">Pets</a>
              <span class="badge"></span>
            </li>
            &lt;%&ndash;<li>
              <a href="<c:out value='${cars_url}'/>">Cars</a>
            </li>&ndash;%&gt;
            <li id="science-menu-item">
              <span class="img"><img src="${baseURL}/resources/img/nav/science.svg" alt="image"/></span>
              <a href="<c:out value='${science_url}'/>">Science</a>
              <span class="badge"></span>
            </li>
            &lt;%&ndash;<li>
              <a href="<c:out value='${seniors_url}'/>">Seniors</a>
            </li>&ndash;%&gt;
            &lt;%&ndash;<li>
              <a href="<c:out value='${shopping_url}'/>">Shopping</a>
            </li>&ndash;%&gt;
            <li id="sports-menu-item">
              <span class="img"><img src="${baseURL}/resources/img/nav/sports.svg" alt="image"/></span>
              <a href="<c:out value='${sports_url}'/>">Sports</a>
              <span class="badge"></span>
            </li>
            <li id="tech-menu-item">
              <span class="img"><img src="${baseURL}/resources/img/nav/tech.svg" alt="image"/></span>
              <a href="<c:out value='${tech_url}'/>">Tech</a>
              <span class="badge"></span>
            </li>
            <li id="travel-menu-item">
              <span class="img"><img src="${baseURL}/resources/img/nav/travel.svg" alt="image"/></span>
              <a href="<c:out value='${travel_url}'/>">Travel</a>
              <span class="badge"></span>
            </li>
            &lt;%&ndash;<li>
              <a href="<c:out value='${tv_url}'/>">TV&Movies</a>
            </li>&ndash;%&gt;
          </ul>--%>
      <%--<iz:navigation />--%>
  </div>
  <span class="hide_menu" value="open"></span>
</side>
<!-- #sideLeft -->
