<%-- 
    Document   : content
    Created on : Feb 25, 2013, 7:23:25 AM
    Author     : work
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%
    String basepath = request.getContextPath();
    double start = System.currentTimeMillis();
%>



<div id="googleblock">
</div>
<div id="analiticblock">

    <div id="crunchapi">

        <div class="crunch_footer">
            <ul>
                <li>          
                    <span>X</span>
                </li>
                <li>          
                    <span>Y</span>
                </li>
                <li>          
                    <span>Z</span>
                </li>
                <li class="last">
                    <span>R</span>
                </li>
            </ul>
            <div class="fix"></div>
        </div>    
    </div>

    <div id="otherapi">
        <div class="widget first wot">
            <div class="head">
                <h5 class="iStats">Other</h5>
            </div>
            <div class="body">
            </div>
        </div>    
    </div>

    <div id="wotapi">
        <div class="widget first wot">
            <div class="head">
                <h5 class="iStats">Reputation</h5>
            </div>
            <div class="body">
            </div>
        </div>    
    </div>

    <div id="competeapi">
        <div class="widget first compete">
            <div class="head">
                <h5 class="iGraph">Unic Visitors</h5>
            </div>
            <div class="body">
                <div id="competechart" class="chart">
                    <canvas class="base"></canvas>
                    <canvas class="overlay"></canvas>
                </div>
            </div>
        </div>    
    </div>

</div>

<!--<div class="SidebarSmall">
    <tiles:insertAttribute name="leftmenu"/>
</div>-->
<!--<div class="ContentSideBarSmall">-->
<div>
    <tiles:insertAttribute name="snippets"/>
<!--    <footer class="Footer">   
        <tiles:insertAttribute name="footer"/>   
    </footer>-->
</div> 
