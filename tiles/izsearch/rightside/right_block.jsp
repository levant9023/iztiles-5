<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<link type="text/css" rel="stylesheet" media="screen" href="${baseURL}/resources/css/izsearch/right_demo.css"/>
<aside id="sideRight" class="right_container col-xs-3 col-sm-3">
	<div class="row">
		<div class="content">
			<div id="infomap">
				<tiles:insertAttribute name="top_news"/>
				<tiles:insertAttribute name="top_news_2"/>
			</div>
			<%--<div id="rss-block"></div>--%>
			<tiles:insertAttribute name="similar-search"/>
			<tiles:insertAttribute name="ads_block"/>
			<tiles:insertAttribute name="paginator"/>
			<div id="googleblock"></div>
			<div id="analiticblock"></div>
		</div>
	</div>
</aside>
