<%--
    Document   : blogblock
    Created on : Dec 20, 2013, 1:38:43 AM
    Author     : work
--%>

<%@page import="org.izsearch.model.SimilarLink"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="t" %>
<%@ taglib prefix="iz" uri="/WEB-INF/tlds/izsearch.tld" %>

<c:set var="ls" value="${requestScope.rss}" />
<c:if test="${not empty ls}" >
    <div class="blogblock wpanel">
        <%
            String query = (String) request.getAttribute("q");
            ArrayList<Object[]> objAl = new ArrayList<>();
            List<SimilarLink> ls = (List<SimilarLink>) request.getAttribute("rss");
            if (ls != null && !ls.isEmpty()) {
                objAl.add(new Object[]{"News", ls});
            }
            /*
             ls = (List<SimilarLinks>) request.getAttribute("blogForum");
             if (ls != null && !ls.isEmpty()) {
             objAl.add(new Object[]{"Blogs", ls});
             }
             ls = (List<SimilarLinks>) request.getAttribute("pubDocPat");
             if (ls != null && !ls.isEmpty()) {
             objAl.add(new Object[]{"Publications, documents, patents", ls});
             }
             ls = (List<SimilarLinks>) request.getAttribute("market");
             if (ls != null && !ls.isEmpty()) {
             objAl.add(new Object[]{"Market", ls});
             }

             */
            for (Object[] objs : objAl) {
        %>

        <c:set var="title" value="<%=objs[0].toString()%>" />
        <c:set var="titleLowerCase" value="${fn:toLowerCase(title)}" />

        <div class="${titleLowerCase}-block">
            <h3 class="underlinedh3"><a href="/news.html?q=${query}&offset=0&records=10">${title}</a></h3>
            <div class="faves" id="${titleLowerCase}-img">
                <div class="newFaves">
                    <div id="tabs" style="border-bottom:0; padding:0">
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="active"><a href="#tabs-52071${titleLowerCase}" role="tab" data-toggle="tab">News</a></li>
                            <li><a href="#tabs-113514${titleLowerCase}" role="tab" data-toggle="tab">Financial news</a></li>
                            <li><a href="#tabs-113519${titleLowerCase}" role="tab" data-toggle="tab">Sport news</a>
                            <li><a href="#tabs-113971${titleLowerCase}" role="tab" data-toggle="tab">Apps</a></li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div id="tabs-52071${titleLowerCase}" class="tab-pane fade in active">
                                <ul id="sortable-52071${titleLowerCase}" class="ui-helper-reset">
                                    <li id="3144585" style="visibility: visible;"
                                        class="cellclass ui-state-default cell"
                                        title="Breaking, World, Business, Sports, Entertainment and Video News">
                                        <a style="background-image:url(http://img0.minifav.net/images/sprite/52071_679.png);display:block;width:70px;height:25px;background-position:0 -25px;" href="http://edition.cnn.com/" target="_blank" onclick="_gaq.push(['_trackPageview','/news/amflinks/mini3/http://edition.cnn.com']);">
                                        </a>
                                    </li>
                                    <li id="3144584" style="visibility: visible;" class="cellclass ui-state-default cell" title="FoxNews.com - Breaking News | Latest News | Current News">
                                        <a style="background-image:url(http://img0.minifav.net/images/sprite/52071_679.png);display:block;width:70px;height:25px;background-position:0 -50px;" href="http://www.foxnews.com/" target="_blank" onclick="_gaq.push(['_trackPageview','/news/amflinks/mini3/http://www.foxnews.com/']);">
                                        </a><img alt="" class="edit_frame hide"  alt="image"/></li>
                                    <li id="3144588" style="visibility: visible;" class="cellclass ui-state-default cell" title="The New York Times - Breaking News, World News &amp; Multimedia">
                                        <a style="background-image:url(http://img0.minifav.net/images/sprite/52071_679.png);display:block;width:70px;height:25px;background-position:0 -75px;" href="http://www.nytimes.com/" target="_blank" onclick="_gaq.push(['_trackPageview','/news/amflinks/mini3/http://www.nytimes.com/']);">
                                        </a><img alt="" class="edit_frame hide" /></li><li id="6301940" style="visibility: visible;" class="cellclass ui-state-default cell" title="msnbc.com - Breaking news, science and tech news, world news, US news, local news- msnbc.com">
                                        <a style="background-image:url(http://img0.minifav.net/images/sprite/52071_679.png);display:block;width:70px;height:25px;background-position:0 -100px;" href="http://www.nbcnews.com/" target="_blank" onclick="_gaq.push(['_trackPageview','/news/amflinks/mini3/http://www.nbcnews.com/']);">
                                        </a><img alt="" class="edit_frame hide" /></li><li id="3144587" style="visibility: visible;" class="cellclass ui-state-default cell" title="Breaking News Headlines: Business, Entertainment &amp; World News">
                                        <a style="background-image:url(http://img0.minifav.net/images/sprite/52071_679.png);display:block;width:70px;height:25px;background-position:0 -125px;" href="http://www.cbsnews.com/" target="_blank" onclick="_gaq.push(['_trackPageview','/news/amflinks/mini3/http://www.cbsnews.com/']);">
                                        </a><img alt="" class="edit_frame hide" /></li><li id="3144592" style="visibility: visible;" class="cellclass ui-state-default cell" title="News, Travel, Weather, Entertainment, Sports, Technology, U.S. &amp; World">
                                        <a style="background-image:url(http://img0.minifav.net/images/sprite/52071_679.png);display:block;width:70px;height:25px;background-position:0 -150px;" href="http://www.usatoday.com/" target="_blank" onclick="_gaq.push(['_trackPageview','/news/amflinks/mini3/http://www.usatoday.com/']);">
                                        </a><img alt="" class="edit_frame hide" /></li><li id="3144593" style="visibility: visible;" class="cellclass ui-state-default cell" title="Los Angeles Times - California, national and world news">
                                        <a style="background-image:url(http://img0.minifav.net/images/sprite/52071_679.png);display:block;width:70px;height:25px;background-position:0 -175px;" href="http://www.latimes.com/" target="_blank" onclick="_gaq.push(['_trackPageview','/news/amflinks/mini3/http://www.latimes.com/']);">
                                        </a><img alt="" class="edit_frame hide" /></li><li id="3144590" style="visibility: visible;" class="cellclass ui-state-default cell" title="Yahoo! News - Latest News &amp; Headlines">
                                        <a style="background-image:url(http://img0.minifav.net/images/sprite/52071_679.png);display:block;width:70px;height:25px;background-position:0 -200px;" href="http://news.yahoo.com/" target="_blank" onclick="_gaq.push(['_trackPageview','/news/amflinks/mini3/http://news.yahoo.com/']);">
                                        </a><img alt="" class="edit_frame hide" /></li><li id="3144589" style="visibility: visible;" class="cellclass ui-state-default cell" title="Google News">
                                        <a style="background-image:url(http://img0.minifav.net/images/sprite/52071_679.png);display:block;width:70px;height:25px;background-position:0 -225px;" href="http://news.google.com/" target="_blank" onclick="_gaq.push(['_trackPageview','/news/amflinks/mini3/http://news.google.com/']);">
                                        </a><img alt="" class="edit_frame hide" /></li><li id="3144594" style="visibility: visible;" class="cellclass ui-state-default cell" title="National, World &amp; D.C. Area News and Headlines - The Washington Post">
                                        <a style="background-image:url(http://img0.minifav.net/images/sprite/52071_679.png);display:block;width:70px;height:25px;background-position:0 -250px;" href="http://www.washingtonpost.com/" target="_blank" onclick="_gaq.push(['_trackPageview','/news/amflinks/mini3/http://www.washingtonpost.com/']);">
                                        </a><img alt="" class="edit_frame hide" /></li><li id="3144595" style="visibility: visible;" class="cellclass ui-state-default cell" title="Chicago breaking news, sports, business, entertainment and weather">
                                        <a style="background-image:url(http://img0.minifav.net/images/sprite/52071_679.png);display:block;width:70px;height:25px;background-position:0 -275px;" href="http://www.chicagotribune.com/" target="_blank" onclick="_gaq.push(['_trackPageview','/news/amflinks/mini3/http://www.chicagotribune.com/']);">
                                        </a><img alt="" class="edit_frame hide" /></li><li id="3144599" style="visibility: visible;" class="cellclass ui-state-default cell" title="BBC - Breaking news, sport, TV, radio and a whole lot more">
                                        <a style="background-image:url(http://img0.minifav.net/images/sprite/52071_679.png);display:block;width:70px;height:25px;background-position:0 -300px;" href="http://www.bbc.co.uk/" target="_blank" onclick="_gaq.push(['_trackPageview','/news/amflinks/mini3/http://www.bbc.co.uk/']);">
                                        </a><img alt="" class="edit_frame hide" /></li><li id="3144602" style="visibility: visible;" class="cellclass ui-state-default cell" title="Breaking News and Opinion on The Huffington Post">
                                        <a style="background-image:url(http://img0.minifav.net/images/sprite/52071_679.png);display:block;width:70px;height:25px;background-position:0 -325px;" href="http://www.huffingtonpost.com/" target="_blank" onclick="_gaq.push(['_trackPageview','/news/amflinks/mini3/http://www.huffingtonpost.com/']);">
                                        </a><img alt="" class="edit_frame hide" /></li><li id="3144597" style="visibility: visible;" class="cellclass ui-state-default cell" title="Boston.com - Boston, MA news, breaking news, sports, video">
                                        <a style="background-image:url(http://img0.minifav.net/images/sprite/52071_679.png);display:block;width:70px;height:25px;background-position:0 -350px;" href="http://www.boston.com/" target="_blank" onclick="_gaq.push(['_trackPageview','/news/amflinks/mini3/http://www.boston.com/']);">
                                        </a><img alt="" class="edit_frame hide" /></li><li id="3144596" style="visibility: visible;" class="cellclass ui-state-default cell" title="Houston news, entertainment, search and shopping | chron.com">
                                        <a style="background-image:url(http://img0.minifav.net/images/sprite/52071_679.png);display:block;width:70px;height:25px;background-position:0 -375px;" href="http://www.chron.com/" target="_blank" onclick="_gaq.push(['_trackPageview','/news/amflinks/mini3/http://www.chron.com/']);">
                                        </a><img alt="" class="edit_frame hide" /></li>
                                </ul>
                            </div>

                            <div id="tabs-113514${titleLowerCase}" class="tab-pane">
                                <ul style="" id="sortable-113514${titleLowerCase}" class=" ui-helper-reset    ">
                                    <li id="4285764" style="visibility: visible;" class="cellclass ui-state-default cell" title="Yahoo! Finance - Business Finance, Stock Market, Quotes, News">
                                        <a style="background-image:url(http://img1.minifav.net/images/sprite/113514_717.png);display:block;width:70px;height:25px;background-position:0 -25px;" href="http://finance.yahoo.com/" target="_blank" onclick="_gaq.push(['_trackPageview','/news/amflinks/mini3/http://finance.yahoo.com/']);">
                                        </a> </li><li id="4285786" style="visibility: visible;" class="cellclass ui-state-default cell" title="Google Finance: Stock market quotes, news, currency conversions &amp; more">
                                        <a style="background-image:url(http://img1.minifav.net/images/sprite/113514_717.png);display:block;width:70px;height:25px;background-position:0 -50px;" href="http://www.finance.google.com/" target="_blank" onclick="_gaq.push(['_trackPageview','/news/amflinks/mini3/http://www.finance.google.com']);">
                                        </a> </li><li id="4285763" style="visibility: visible;" class="cellclass ui-state-default cell" title="Europe Edition - Wall Street Journal - Latest News, Breaking Stories, Top Headlines - Wsj.com">
                                        <a style="background-image:url(http://img1.minifav.net/images/sprite/113514_717.png);display:block;width:70px;height:25px;background-position:0 -75px;" href="http://europe.wsj.com/home-page" target="_blank" onclick="_gaq.push(['_trackPageview','/news/amflinks/mini3/http://europe.wsj.com/home-page']);">
                                        </a> </li><li id="4285765" style="visibility: visible;" class="cellclass ui-state-default cell" title="Businessweek - Business News, Stock Market &amp; Financial Advice">
                                        <a style="background-image:url(http://img1.minifav.net/images/sprite/113514_717.png);display:block;width:70px;height:25px;background-position:0 -100px;" href="http://www.businessweek.com/" target="_blank" onclick="_gaq.push(['_trackPageview','/news/amflinks/mini3/http://www.businessweek.com/']);">
                                        </a> </li><li id="4285766" style="visibility: visible;" class="cellclass ui-state-default cell" title="Stock Market News, Business News, Financial, Earnings, World Market News and Information - CNBC">
                                        <a style="background-image:url(http://img1.minifav.net/images/sprite/113514_717.png);display:block;width:70px;height:25px;background-position:0 -125px;" href="http://www.cnbc.com/" target="_blank" onclick="_gaq.push(['_trackPageview','/news/amflinks/mini3/http://www.cnbc.com/']);">
                                        </a> </li><li id="4285767" style="visibility: visible;" class="cellclass ui-state-default cell" title="Home - FoxBusiness.com">
                                        <a style="background-image:url(http://img1.minifav.net/images/sprite/113514_717.png);display:block;width:70px;height:25px;background-position:0 -150px;" href="http://www.foxbusiness.com/index.html" target="_blank" onclick="_gaq.push(['_trackPageview','/news/amflinks/mini3/http://www.foxbusiness.com/index.html']);">
                                        </a> </li><li id="4285769" style="visibility: visible;" class="cellclass ui-state-default cell" title="Fortune 500 Daily &amp; Breaking Business News - FORTUNE on CNNMoney">
                                        <a style="background-image:url(http://img1.minifav.net/images/sprite/113514_717.png);display:block;width:70px;height:25px;background-position:0 -175px;" href="http://money.cnn.com/magazines/fortune/" target="_blank" onclick="_gaq.push(['_trackPageview','/news/amflinks/mini3/http://money.cnn.com/magazines/fortune/']);">
                                        </a> </li><li id="14364746" style="visibility: visible;" class="cellclass ui-state-default cell" title="Information for the World's Business Leaders - Forbes.com">
                                        <a style="background-image:url(http://img1.minifav.net/images/sprite/113514_717.png);display:block;width:70px;height:25px;background-position:0 -200px;" href="http://www.forbes.com/" target="_blank" onclick="_gaq.push(['_trackPageview','/news/amflinks/mini3/http://www.forbes.com/']);">
                                        </a> </li><li id="14364748" style="visibility: visible;" class="cellclass ui-state-default cell" title="CNNMoney - Business, financial and personal finance news">
                                        <a style="background-image:url(http://img1.minifav.net/images/sprite/113514_717.png);display:block;width:70px;height:25px;background-position:0 -225px;" href="http://money.cnn.com/" target="_blank" onclick="_gaq.push(['_trackPageview','/news/amflinks/mini3/http://money.cnn.com/']);">
                                        </a> </li><li id="4285774" style="visibility: visible;" class="cellclass ui-state-default cell" title="msnbc.com - Business news, jobs news, financial news, stock market news, real estate news, finance tips- msnbc.com">
                                        <a style="background-image:url(http://img1.minifav.net/images/sprite/113514_717.png);display:block;width:70px;height:25px;background-position:0 -250px;" href="http://www.msnbc.msn.com/id/3032072/ns/business/" target="_blank" onclick="_gaq.push(['_trackPageview','/news/amflinks/mini3/http://www.msnbc.msn.com/id/3032072/ns/business/']);">
                                        </a> </li><li id="14364752" style="visibility: visible;" class="cellclass ui-state-default cell" title="Bloomberg - Business, Financial &amp; Economic News, Stock Quotes">
                                        <a style="background-image:url(http://img1.minifav.net/images/sprite/113514_717.png);display:block;width:70px;height:25px;background-position:0 -275px;" href="http://www.bloomberg.com/" target="_blank" onclick="_gaq.push(['_trackPageview','/news/amflinks/mini3/http://www.bloomberg.com/']);">
                                        </a> </li><li id="4285776" style="visibility: visible;" class="cellclass ui-state-default cell" title="MarketWatch - Stock Market Quotes, Business News, Financial News">
                                        <a style="background-image:url(http://img1.minifav.net/images/sprite/113514_717.png);display:block;width:70px;height:25px;background-position:0 -300px;" href="http://www.marketwatch.com/" target="_blank" onclick="_gaq.push(['_trackPageview','/news/amflinks/mini3/http://www.marketwatch.com/']);">
                                        </a> </li><li id="4285778" style="visibility: visible;" class="cellclass ui-state-default cell" title="World business, finance and political news from the Financial Times– FT.com ">
                                        <a style="background-image:url(http://img1.minifav.net/images/sprite/113514_717.png);display:block;width:70px;height:25px;background-position:0 -325px;" href="http://www.ft.com/" target="_blank" onclick="_gaq.push(['_trackPageview','/news/amflinks/mini3/http://www.ft.com/']);">
                                        </a> </li><li id="4285779" style="visibility: visible;" class="cellclass ui-state-default cell" title="Business and Financial News - The New York Times">
                                        <a style="background-image:url(http://img1.minifav.net/images/sprite/113514_717.png);display:block;width:70px;height:25px;background-position:0 -350px;" href="http://www.nytimes.com/pages/business/" target="_blank" onclick="_gaq.push(['_trackPageview','/news/amflinks/mini3/http://www.nytimes.com/pages/business/']);">
                                        </a> </li><li id="14364749" style="visibility: visible;" class="cellclass ui-state-default cell" title="The Economist - World News, Politics, Economics, Business &amp; Finance">
                                        <a style="background-image:url(http://img1.minifav.net/images/sprite/113514_717.png);display:block;width:70px;height:25px;background-position:0 -375px;" href="http://www.economist.com/" target="_blank" onclick="_gaq.push(['_trackPageview','/news/amflinks/mini3/http://www.economist.com/']);">
                                        </a> </li>
                                </ul>
                            </div>
                            <div id="tabs-113519${titleLowerCase}" class="tab-pane">
                                <ul style="" id="sortable-113519${titleLowerCase}" class=" ui-helper-reset    ">
                                    <li id="4285864" style="visibility: visible;" class="cellclass ui-state-default cell" title="ESPN: The Worldwide Leader In Sports">
                                        <a style="background-image:url(http://img2.minifav.net/images/sprite/113519_294.png);display:block;width:70px;height:25px;background-position:0 -25px;" href="http://espn.go.com/" target="_blank" onclick="_gaq.push(['_trackPageview','/news/amflinks/mini3/http://espn.go.com/']);">
                                        </a> </li><li id="4285833" style="visibility: visible;" class="cellclass ui-state-default cell" title="FOX Sports on MSN l Sports News, Scores, Schedules, Videos">
                                        <a style="background-image:url(http://img2.minifav.net/images/sprite/113519_294.png);display:block;width:70px;height:25px;background-position:0 -50px;" href="http://msn.foxsports.com/" target="_blank" onclick="_gaq.push(['_trackPageview','/news/amflinks/mini3/http://msn.foxsports.com/']);">
                                        </a> </li><li id="4285832" style="visibility: visible;" class="cellclass ui-state-default cell" title="Breaking news, real-time scores and daily analysis - SI.com">
                                        <a style="background-image:url(http://img2.minifav.net/images/sprite/113519_294.png);display:block;width:70px;height:25px;background-position:0 -75px;" href="http://sportsillustrated.cnn.com/" target="_blank" onclick="_gaq.push(['_trackPageview','/news/amflinks/mini3/http://sportsillustrated.cnn.com/']);">
                                        </a> </li><li id="4285834" style="visibility: visible;" class="cellclass ui-state-default cell" title="Sports News Headlines - NFL, NBA, NHL, MLB, PGA, NASCAR">
                                        <a style="background-image:url(http://img2.minifav.net/images/sprite/113519_294.png);display:block;width:70px;height:25px;background-position:0 -100px;" href="http://nbcsports.msnbc.com/" target="_blank" onclick="_gaq.push(['_trackPageview','/news/amflinks/mini3/http://nbcsports.msnbc.com/']);">
                                        </a> </li><li id="4285835" style="visibility: visible;" class="cellclass ui-state-default cell" title="Yahoo! Sports - Sports News, Scores, Rumors, Fantasy Games">
                                        <a style="background-image:url(http://img2.minifav.net/images/sprite/113519_294.png);display:block;width:70px;height:25px;background-position:0 -125px;" href="http://sports.yahoo.com/" target="_blank" onclick="_gaq.push(['_trackPageview','/news/amflinks/mini3/http://sports.yahoo.com/']);">
                                        </a> </li><li id="4285859" style="visibility: visible;" class="cellclass ui-state-default cell" title="Bleacher Report | Entertaining sports news, photos and slideshows">
                                        <a style="background-image:url(http://img2.minifav.net/images/sprite/113519_294.png);display:block;width:70px;height:25px;background-position:0 -150px;" href="http://bleacherreport.com/" target="_blank" onclick="_gaq.push(['_trackPageview','/news/amflinks/mini3/http://bleacherreport.com/']);">
                                        </a> </li><li id="4285836" style="visibility: visible;" class="cellclass ui-state-default cell" title="NBA.com">
                                        <a style="background-image:url(http://img2.minifav.net/images/sprite/113519_294.png);display:block;width:70px;height:25px;background-position:0 -175px;" href="http://www.nba.com/" target="_blank" onclick="_gaq.push(['_trackPageview','/news/amflinks/mini3/http://www.nba.com/']);">
                                        </a> </li><li id="4285837" style="visibility: visible;" class="cellclass ui-state-default cell" title="NFL.com - Official Site of the National Football League">
                                        <a style="background-image:url(http://img2.minifav.net/images/sprite/113519_294.png);display:block;width:70px;height:25px;background-position:0 -200px;" href="http://www.nfl.com/" target="_blank" onclick="_gaq.push(['_trackPageview','/news/amflinks/mini3/http://www.nfl.com/']);">
                                        </a> </li><li id="4285838" style="visibility: visible;" class="cellclass ui-state-default cell" title="The Official Site of Major League Baseball | MLB.com">
                                        <a style="background-image:url(http://img2.minifav.net/images/sprite/113519_294.png);display:block;width:70px;height:25px;background-position:0 -225px;" href="http://mlb.mlb.com/" target="_blank" onclick="_gaq.push(['_trackPageview','/news/amflinks/mini3/http://mlb.mlb.com']);">
                                        </a> </li><li id="4285839" style="visibility: visible;" class="cellclass ui-state-default cell" title="The Official Site of the PGA TOUR - PGATOUR.com">
                                        <a style="background-image:url(http://img2.minifav.net/images/sprite/113519_294.png);display:block;width:70px;height:25px;background-position:0 -250px;" href="http://www.pgatour.com/" target="_blank" onclick="_gaq.push(['_trackPageview','/news/amflinks/mini3/http://www.pgatour.com/']);">
                                        </a> </li><li id="4285840" style="visibility: visible;" class="cellclass ui-state-default cell" title="US Open Official Site">
                                        <a style="background-image:url(http://img2.minifav.net/images/sprite/113519_294.png);display:block;width:70px;height:25px;background-position:0 -275px;" href="http://www.usopen.org/" target="_blank" onclick="_gaq.push(['_trackPageview','/news/amflinks/mini3/http://www.usopen.org/']);">
                                        </a> </li><li id="4285841" style="visibility: visible;" class="cellclass ui-state-default cell" title="CBSSports.com Sports News, Fantasy Scores, Sports Video">
                                        <a style="background-image:url(http://img2.minifav.net/images/sprite/113519_294.png);display:block;width:70px;height:25px;background-position:0 -300px;" href="http://www.cbssports.com/" target="_blank" onclick="_gaq.push(['_trackPageview','/news/amflinks/mini3/http://www.cbssports.com/']);">
                                        </a> </li><li id="4285842" style="visibility: visible;" class="cellclass ui-state-default cell" title="Sporting News - Real Insight. Real Fans. Real Conversations">
                                        <a style="background-image:url(http://img2.minifav.net/images/sprite/113519_294.png);display:block;width:70px;height:25px;background-position:0 -325px;" href="http://www.sportingnews.com/" target="_blank" onclick="_gaq.push(['_trackPageview','/news/amflinks/mini3/http://www.sportingnews.com/']);">
                                        </a> </li><li id="4285843" style="visibility: visible;" class="cellclass ui-state-default cell" title="SPEED - The Motor Sports Authority">
                                        <a style="background-image:url(http://img2.minifav.net/images/sprite/113519_294.png);display:block;width:70px;height:25px;background-position:0 -350px;" href="http://www.speedtv.com/" target="_blank" onclick="_gaq.push(['_trackPageview','/news/amflinks/mini3/http://www.speedtv.com/']);">
                                        </a> </li><li id="4285844" style="visibility: visible;" class="cellclass ui-state-default cell" title="Deadspin, Sports News without Access, Favor, or Discretion">
                                        <a style="background-image:url(http://img2.minifav.net/images/sprite/113519_294.png);display:block;width:70px;height:25px;background-position:0 -375px;" href="http://deadspin.com/" target="_blank" onclick="_gaq.push(['_trackPageview','/news/amflinks/mini3/http://deadspin.com/']);">
                                        </a> </li></ul>
                            </div>
                            <div id="tabs-113971${titleLowerCase}" class="tab-pane">
                                <ul style="" id="sortable-113971${titleLowerCase}" class=" ui-helper-reset    ">
                                    <li id="4297135" style="visibility: visible;" class="cellclass ui-state-default cell" title="CNN mobile app">
                                        <a style="background-image:url(http://img3.minifav.net/images/sprite/113971_898.png);display:block;width:70px;height:25px;background-position:0 -25px;" href="http://edition.cnn.com/mobile" target="_blank" onclick="_gaq.push(['_trackPageview','/news/amflinks/mini3/http://edition.cnn.com/mobile']);">
                                        </a> </li><li id="4297136" style="visibility: visible;" class="cellclass ui-state-default cell" title="Mobile - FoxNews.com">
                                        <a style="background-image:url(http://img3.minifav.net/images/sprite/113971_898.png);display:block;width:70px;height:25px;background-position:0 -50px;" href="http://www.foxnews.com/mobile/" target="_blank" onclick="_gaq.push(['_trackPageview','/news/amflinks/mini3/http://www.foxnews.com/mobile/']);">
                                        </a> </li><li id="4297131" style="visibility: visible;" class="cellclass ui-state-default cell" title="Mobile News Apps | Reuters.com">
                                        <a style="background-image:url(http://img3.minifav.net/images/sprite/113971_898.png);display:block;width:70px;height:25px;background-position:0 -75px;" href="http://www.reuters.com/tools/mobile" target="_blank" onclick="_gaq.push(['_trackPageview','/news/amflinks/mini3/http://www.reuters.com/tools/mobile']);">
                                        </a> </li><li id="4297133" style="visibility: visible;" class="cellclass ui-state-default cell" title="Mobile - Apps - The New York Times">
                                        <a style="background-image:url(http://img3.minifav.net/images/sprite/113971_898.png);display:block;width:70px;height:25px;background-position:0 -100px;" href="http://www.nytimes.com/services/mobile/apps/" target="_blank" onclick="_gaq.push(['_trackPageview','/news/amflinks/mini3/http://www.nytimes.com/services/mobile/apps/']);">
                                        </a> </li><li id="4297163" style="visibility: visible;" class="cellclass ui-state-default cell" title="AP Mobile">
                                        <a style="background-image:url(http://img3.minifav.net/images/sprite/113971_898.png);display:block;width:70px;height:25px;background-position:0 -125px;" href="http://www.getapmobile.com/" target="_blank" onclick="_gaq.push(['_trackPageview','/news/amflinks/mini3/http://www.getapmobile.com/']);">
                                        </a> </li><li id="4297424" style="visibility: visible;" class="cellclass ui-state-default cell" title="ABC News Mobile - ABC News">
                                        <a style="background-image:url(http://img3.minifav.net/images/sprite/113971_898.png);display:block;width:70px;height:25px;background-position:0 -150px;" href="http://abcnews.go.com/Technology/page?id=6481871" target="_blank" onclick="_gaq.push(['_trackPageview','/news/amflinks/mini3/http://abcnews.go.com/Technology/page?id=6481871']);">
                                        </a> </li><li id="4297434" style="visibility: visible;" class="cellclass ui-state-default cell" title="Mobile News Apps: Android, iPhone, iPad, and more – USATODAY.com">
                                        <a style="background-image:url(http://img3.minifav.net/images/sprite/113971_898.png);display:block;width:70px;height:25px;background-position:0 -175px;" href="http://www.usatoday.com/mobile/index.htm" target="_blank" onclick="_gaq.push(['_trackPageview','/news/amflinks/mini3/http://www.usatoday.com/mobile/index.htm']);">
                                        </a> </li><li id="4297436" style="visibility: visible;" class="cellclass ui-state-default cell" title="Los Angeles Times | Mobile - latimes.com">
                                        <a style="background-image:url(http://img3.minifav.net/images/sprite/113971_898.png);display:block;width:70px;height:25px;background-position:0 -200px;" href="http://www.latimes.com/services/site/mobile/" target="_blank" onclick="_gaq.push(['_trackPageview','/news/amflinks/mini3/http://www.latimes.com/services/site/mobile/']);">
                                        </a> </li><li id="4297433" style="visibility: visible;" class="cellclass ui-state-default cell" title="Mobile WSJ - Get Wall Street Journal on your Iphone, Cell Phone, &amp; Blackberry - WSJ.com">
                                        <a style="background-image:url(http://img3.minifav.net/images/sprite/113971_898.png);display:block;width:70px;height:25px;background-position:0 -225px;" href="http://online.wsj.com/public/page/mobile.html" target="_blank" onclick="_gaq.push(['_trackPageview','/news/amflinks/mini3/http://online.wsj.com/public/page/mobile.html']);">
                                        </a> </li><li id="4297628" style="visibility: visible;" class="cellclass ui-state-default cell" title="CNBC 360 - CNBC">
                                        <a style="background-image:url(http://img3.minifav.net/images/sprite/113971_898.png);display:block;width:70px;height:25px;background-position:0 -250px;" href="http://classic.cnbc.com/id/33077961/" target="_blank" onclick="_gaq.push(['_trackPageview','/news/amflinks/mini3/http://classic.cnbc.com/id/33077961/']);">
                                        </a> </li><li id="4297422" style="visibility: visible;" class="cellclass ui-state-default cell" title="NPR Mobile Home">
                                        <a style="background-image:url(http://img3.minifav.net/images/sprite/113971_898.png);display:block;width:70px;height:25px;background-position:0 -275px;" href="http://www.npr.org/services/mobile/" target="_blank" onclick="_gaq.push(['_trackPageview','/news/amflinks/mini3/http://www.npr.org/services/mobile/']);">
                                        </a> </li><li id="4297423" style="visibility: visible;" class="cellclass ui-state-default cell" title="Weather for Apple iPhone Smartphones - WeatherBug">
                                        <a style="background-image:url(http://img3.minifav.net/images/sprite/113971_898.png);display:block;width:70px;height:25px;background-position:0 -300px;" href="http://weather.weatherbug.com/mobile/weatherbug-for-iphone.html" target="_blank" onclick="_gaq.push(['_trackPageview','/news/amflinks/mini3/http://weather.weatherbug.com/mobile/weatherbug-for-iphone.html']);">
                                        </a> </li><li id="4297425" style="visibility: visible;" class="cellclass ui-state-default cell" title="Top News - Fluent News">
                                        <a style="background-image:url(http://img3.minifav.net/images/sprite/113971_898.png);display:block;width:70px;height:25px;background-position:0 -325px;" href="http://www.fluentnews.com/" target="_blank" onclick="_gaq.push(['_trackPageview','/news/amflinks/mini3/http://www.fluentnews.com/']);">
                                        </a> </li><li id="4297601" style="visibility: visible;" class="cellclass ui-state-default cell" title="Pulse News - by Alphonso Labs">
                                        <a style="background-image:url(http://img3.minifav.net/images/sprite/113971_898.png);display:block;width:70px;height:25px;background-position:0 -350px;" href="http://www.pulse.me/" target="_blank" onclick="_gaq.push(['_trackPageview','/news/amflinks/mini3/http://www.pulse.me/']);">
                                        </a> </li><li id="4297599" style="visibility: visible;" class="cellclass ui-state-default cell" title="Flipboard for iPad">
                                        <a style="background-image:url(http://img3.minifav.net/images/sprite/113971_898.png);display:block;width:70px;height:25px;background-position:0 -375px;" href="http://flipboard.com/" target="_blank" onclick="_gaq.push(['_trackPageview','/news/amflinks/mini3/http://flipboard.com/']);">
                                        </a> </li></ul>
                            </div>
                        </div>

                        <c:set var="news_list" value="${requestScope.rss}" />
                        <c:forEach var="item" items="${news_list}" varStatus="newstatus">
                            <article class="art">
                                <c:set var="title_" value="${iz:text_normalize(item.firstLink.title)}" />
                                <c:set var="title" value="${iz:strip_str_pxl(title_,' ', 'Arial', 14, 400)}" />
                                <c:url var="uri" value="${item.firstLink.url}"/>
                                <h3 class="art-news-itle"><a href="${uri}">${title}</a></h3>
                                <div class="art-news-sublink">
                                    <a href="${uri}">
                                        <c:set var="sublink" value="${iz:get_domain(uri)}" />
                                        <c:set var="date" value="${item.firstLink.date}" />
                                        <span class="art-news-date">${date}</span>
                                        <span class="art-news-sep">-</span>
                                        <span class="art-news-link-decor">${sublink}</span>
                                    </a>
                                </div>
                                <p class="art-news-descr">
                                    <c:set var="descr" value="${iz:text_normalize(item.firstLink.description)}" />
                                    <c:set var="descr" value="${iz:strip_str_pxl(descr, ' ', 'Arial', 13, 910)}" />
                                    <c:out value="${descr}" default="N/A"/>
                                </p>
                            </article>
                        </c:forEach>
                    </div>
                    <%}%>
                </div>
            </c:if>
