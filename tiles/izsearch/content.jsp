<%-- 
    Document   : content
    Created on : Feb 25, 2013, 7:23:25 AM
    Author     : work
--%>
<%@page import="com.biosearch.index.client.TextIdWithRank"%>
<%@page import="org.izsearch.helper.HtmlEntities"%>
<%@page import="org.izsearch.helper.HtmlStrUtils"%>
<%@page import="org.izsearch.helper.StringUtils"%>
<%@page import="org.izsearch.model.LinkAndImages"%>
<%@page import="org.izsearch.model.SimilarLink"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%
    String basepath = request.getContextPath();   
%>

<div id="mapblock">
    <div id="map"></div>
    <div id=infopanel>
        <div style="margin-bottom: 40px">
            <b>Images</b><br>
            <%
                List<LinkAndImages> images = (List<LinkAndImages>) request.getAttribute("images");
                if (images != null) {
                    int i = 0;
                    top:
                    for (LinkAndImages li : images) {
                        for (String image : li.getImages()) {
            %><img class="PreviewImg" style="max-height: 120px;max-width: 120px;"src="<%=basepath + image%>" alt="image"/>&nbsp;<%
                            i++;
                            if (i == 3) {
                                break top;
                            }
                        }
                    }
                }
            %>
        </div>
        <%           String query = (String) request.getAttribute("q");
            ArrayList<Object[]> objAl = new ArrayList<Object[]>();
            List<SimilarLink> ls = (List<SimilarLink>) request.getAttribute("rss");
            if (ls != null && !ls.isEmpty()) {
                objAl.add(new Object[]{"News", ls});
            }
            ls = (List<SimilarLink>) request.getAttribute("blogForum");
            if (ls != null && !ls.isEmpty()) {
                objAl.add(new Object[]{"Blogs", ls});
            }
            ls = (List<SimilarLink>) request.getAttribute("pubDocPat");
            if (ls != null && !ls.isEmpty()) {
                objAl.add(new Object[]{"Publications, documents, patents", ls});
            }
            ls = (List<SimilarLink>) request.getAttribute("market");
            if (ls != null && !ls.isEmpty()) {
                objAl.add(new Object[]{"Market", ls});
            }
            for (Object[] objs : objAl) {
        %>
        <div style="margin-bottom: 40px">
            <b><%=objs[0].toString()%></b><br>
            <%
                List<SimilarLink> rss = (List<SimilarLink>) objs[1];
                for (int j = 0; j < rss.size(); j++) {
                    SimilarLink ti = rss.get(j);
                    TextIdWithRank t = ti.getFirstLink();

                    String HideOrShow = "";
                    String ListOrGrid = "Article";
                    String articletitle = "ArticleTitle";
                    String analitic = "Evaluate credibility and popularity of www.cancer.org from Analytics.";
                    String similar = "Show similar pages from the same domain.";

                    String url = t.getUrl();
                    String a_nod = "";
                    String sublink = "";
                    String subtitle = "";
                    String fsubtitle = "";
                    String descr = "";
                    String descr2 = "";
                    String desc_max = "";
                    String desc_hint = "";
                    String desc_min = "";


                    // If a length of url is great than 65 letters
                    // strip it and add separator "/"
                    if (url.length() > 65) {
                        url = StringUtils.stripStr(url, "/", 65);
                    }

                    // replace scheme in url
                    url = url.replaceFirst("^http:\\/\\/", "");
                    url = url.replaceAll("\\/$", "");


                    // Set a subtitle for article snippet with 
                    // first capitalized token in words
                    if (!StringUtils.isEmpty(t.getTitle())) {
                        subtitle = HtmlEntities.htmlentities(StringUtils.clearify(StringUtils.ucwords(t.getTitle(), "\u0020")));
                        subtitle = StringUtils.stripStrByPixel(subtitle, '\u0020', 520, 1, "Arial", 16);
                    }

                    subtitle = HtmlEntities.htmlQuotes(subtitle);
                    fsubtitle = subtitle;

                    // check description (null or empty)
                    if (!StringUtils.isEmpty(t.getDescription())) {
                        descr = HtmlEntities.htmlentities(StringUtils.clearify(t.getDescription()));
                        descr = HtmlEntities.htmlQuotes(descr);
                        //descr = HtmlStrUtils.markRegWords(descr, query);
                    }

                    // check other description (null or empty)
                    if (!StringUtils.isEmpty(t.getTextDescription())) {
                        descr2 = HtmlEntities.htmlentities(StringUtils.clearify(t.getTextDescription()));
                        descr2 = HtmlEntities.htmlQuotes(descr2);
                    }

                    String[] ds = new String[2];
                    ds = StringUtils.testDescr(descr, descr2);
                    descr = ds[0];
                    descr2 = ds[1];

                    // save conjunction of two description       
                    String br = "<br />";
                    if (StringUtils.isEmpty(descr)) {
                        br = "";
                        descr = descr2;
                        desc_max = descr2;
                    } else {
                        if (!descr.endsWith(".")) {
                            descr += ".";
                        }
                        if (!StringUtils.isEmpty(descr2)) {
                            if (descr2.length() < 3) {
                                descr2 = "";
                            };

                            desc_max = descr + br + descr2;
                            int desclen = (int) StringUtils.getStrSizePx(descr, "Arial", 13);
                            if (desclen < 550) {
                                descr = descr + "<br />" + descr2;
                            }
                        }

                    }


                    desc_max = HtmlEntities.htmlQuotes(desc_max);

                    // prepare for a hint 
                    if (!StringUtils.isEmpty(subtitle)) {
                        br = ".<br />";
                        desc_hint = "<b>" + HtmlEntities.htmlQuotes(subtitle.trim()) + "</b>" + br;
                    } else {
                        br = "";
                        desc_hint = HtmlEntities.htmlQuotes(subtitle.trim()) + br;
                    }

                    if (!StringUtils.isEmpty(descr)) {
                        br = "<br />";
                        desc_hint += HtmlEntities.htmlQuotes(desc_max.trim()) + br;
                    } else {
                        br = "";
                        desc_hint += HtmlEntities.htmlQuotes(desc_max.trim()) + br;
                    }

                    subtitle = StringUtils.stripStrByPixel(subtitle, '\u0020', 550, 1, "Arial", 16);
                    descr = StringUtils.stripStrByPixel(descr, '\u0020', 550, 2, "Arial", 13);

                    desc_hint = HtmlStrUtils.markRegWords(desc_hint, query);
                    desc_min = HtmlStrUtils.markRegWords(descr, query);
                    subtitle = HtmlStrUtils.markRegWords(subtitle, query);

                    String date = t.getDate();
                    if (date != null && date.length() > 0) {
                        date = date.substring(0, 4) + "." + date.substring(4, 6) + "." + date.substring(6, 8);
                    }
            %>
            <article title="<%=desc_hint%>" class="<%=ListOrGrid%>" style="<%=HideOrShow%>" data-name="art<%=j%>">
                <h3 class="<%=articletitle%>"><a class="<%=a_nod%>" href="<%=url%>"><%=subtitle%></a></h3>  
                <span class="ArticleSubtitle <%=sublink%>">
                    <span style="color: grey"><%=date%></span>     
                    <span style="color: green"><%=HtmlStrUtils.markRegWords(url, query)%></span>     
                </span> 
                <%--
                String html = "&nbsp;-&nbsp;";
                html += "<span class='analitic' title=\"" + analitic + "\"><a href=''>Analitics</a></span>";
                html += "&nbsp;-&nbsp;";
                html += "<span class='asimilar' title=\"" + similar + "\"><a href=''>Similar</a></span>";
                out.print(html);

                --%>      
                <p <%=j > 0 ? "style=\"display:none\"" : ""%> title="<%=desc_hint%>" data-descmin="<%=desc_min%>" data-descmax="<%=desc_max%>">
                    <%=desc_min%>
                </p>
            </article>
            <!--<div class="CrawlerPreview">
              <h3 class="Uncolored">
                <a href="<%=t.getUrl()%>" style="color: #438DD0;text-decoration: none;" onclick="onAClick('<%=t.getUrl()%>'); return false">
            <%=t.getTitle()%>
          </a>
    
    
        </h3>
    
    
        <div><%=descr%></div>
    
      </div>-->
            <%
                }%>
        </div>
        <%}
        %>
    </div>  

</div>

<div id="googleblock">
</div>
<div id="analiticblock">

    <div id="crunchapi">

        <div class="crunch_footer">
            <ul>
                <li>          
                    <span>X</span>
                </li>
                <li>          
                    <span>Y</span>
                </li>
                <li>          
                    <span>Z</span>
                </li>
                <li class="last">
                    <span>R</span>
                </li>
            </ul>
            <div class="fix"></div>
        </div>    
    </div>

    <div id="otherapi">
        <div class="widget first wot">
            <div class="head">
                <h5 class="iStats">Other</h5>
            </div>
            <div class="body">
            </div>
        </div>    
    </div>

    <div id="wotapi">
        <div class="widget first wot">
            <div class="head">
                <h5 class="iStats">Reputation</h5>
            </div>
            <div class="body">
            </div>
        </div>    
    </div>

    <div id="competeapi">
        <div class="widget first compete">
            <div class="head">
                <h5 class="iGraph">Unic Visitors</h5>
            </div>
            <div class="body">
                <div id="competechart" class="chart">
                    <canvas class="base"></canvas>
                    <canvas class="overlay"></canvas>
                </div>
            </div>
        </div>    
    </div>

</div>
<div id="frameblock" style="display:none">
    <span class="spbtn_close" style="display: none">
        <button id="btf_close" title="Close frame">X</button>&nbsp;&nbsp;
        <button id="btf_zoomin" title="Zoom in">+</button>
        <button id="btf_zoomout" title="Zoom out">-</button>
    </span>
    <span class="img_frame_oc" style="display: none">
        <button id="btf_oc" title="Expand/Collapse">&lt;&lt;</button>         
    </span>
    <iframe id="if_get_html" scrolling="no" security="restricted"></iframe>
</div> 
<div id="btns_top_right">
    <span><button id="btn_google">Compare Google</button></span>
    <span><button id="btn_forum">Discussion forum</button></span>
</div>
<div class="SidebarSmall">
    <tiles:insertAttribute name="leftmenu"/>
</div>
<div class="ContentSideBarSmall">
    <tiles:insertAttribute name="snippets"/>
    <footer class="Footer">   
        <tiles:insertAttribute name="footer"/>   
    </footer>
</div> 
