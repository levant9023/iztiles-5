<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<c:url var="video_url" value="/video.html">
	<c:param name="q" value="${requestScope.q}" />
</c:url>
<script type="text/javascript" src="${baseURL}/resources/js/production/photoZoom.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.css" />
<link type="text/css" rel="stylesheet" media="screen" href="${baseURL}/resources/css/izsearch/rightads.css"/>
<aside id="sideRightAds" class="center_container col-xs-10 col-sm-10">
  <div id="rsads-wrapper" class="row">
    <div id="rsads-container" class="content">
      <div id="mapblock1"></div>
      <div class="moreInfo"></div>
      <tiles:insertAttribute name="wikiblock"/>
      <tiles:insertAttribute name="bangs"/>
      <div id="adunit0">
        <script type="text/javascript">
          var query = '${requestScope.q}';
          amzn_assoc_placement = "adunit0";
          amzn_assoc_search_bar = "false";
          amzn_assoc_tracking_id = "izsearchcom0c-20";
          amzn_assoc_search_bar_position = "bottom";
          amzn_assoc_ad_mode = "search";
          amzn_assoc_ad_type = "smart";
          amzn_assoc_marketplace = "amazon";
          amzn_assoc_region = "US";
          amzn_assoc_title = "";
          amzn_assoc_default_search_phrase = query;
          amzn_assoc_default_category = "All";
          amzn_assoc_linkid = "5c74723557905a17a40bd758b812bbe3";
          amzn_assoc_rows = "1";
        </script>
        <script src="//z-na.amazon-adsystem.com/widgets/onejs?MarketPlace=US"></script>
      </div>
      <div id="mapblock2"></div>
      <div id="videop" class="blogblock wpanel">
        <div id="video-text-block"></div>
        <div id="video-bing-block"></div>
        <div id="video-faves-block"></div>
      </div>
      <tiles:insertAttribute name="ali"/>
      <tiles:insertAttribute name="keywords"/>
      <tiles:insertAttribute name="faves"/>
      <div id="amazon-wrapper"></div>
    </div>
  </div>
</aside>

<%--<script type="text/javascript">
  $(document).ready(function () {
    var hover_timeout = 0;
    $('#mapblock2').hover(function () {
        clearTimeout(hover_timeout);
        hover_timeout = setTimeout(function () {
            $('#owl-images').slick('slickNext');
        }, 3000);
    });
  });
</script>--%>
