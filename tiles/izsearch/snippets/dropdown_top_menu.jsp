<div id="moreMenu" class="dropdown">
  <button id="moreMenuButton" class="btn btn-default dropdown-toggle" type="button" title="Discover business news, technical reviews, recipes, travel, style and other ideas to try." data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"><img src="${baseURL}/resources/img/nav/menu.png" class="icon-inert"></button>
  <ul class="dropdown-menu" aria-labelledby="moreMenuButton">
    <span class="moreMenu_title">Explore Thematic Pages</span>
    <li><a href="/art.html"><img src="/resources/img/nav/art.svg"><span>Art</span></a></li>
    <li><a href="/blogs_magazines.html"><img src="/resources/img/nav/blogs_magazines.svg"><span>Blogs & Magazines</span></a></li>
    <li><a href="/books.html"><img src="/resources/img/nav/books.svg"><span>Books</span></a></li>
    <li><a href="/business.html"><img src="/resources/img/nav/business.svg"><span>Business</span></a></li>
    <li><a href="/cars.html"><img src="/resources/img/nav/cars.svg"><span>Cars</span></a></li>
    <li><a href="/celebrity.html"><img src="/resources/img/nav/celebrity.svg"><span>Celebrity</span></a></li>
    <li><a href="/craft.html"><img src="/resources/img/nav/craft.svg"><span>Craft</span></a></li>
    <li><a href="/design.html"><img src="/resources/img/nav/design.svg"><span>Design</span></a></li>
    <li><a href="/education.html"><img src="/resources/img/nav/education.svg"><span>Education</span></a></li>
    <li><a href="/family.html"><img src="/resources/img/nav/family.svg"><span>Family</span></a></li>
    <li><a href="/food_recipes.html"><img src="/resources/img/nav/food_recipes.svg"><span>Food & Recipes</span></a></li>
    <li><a href="/fun.html"><img src="/resources/img/nav/fun.svg"><span>Fun</span></a></li>
    <li><a href="/gifts.html"><img src="/resources/img/nav/gifts.svg"><span>Gifts</span></a></li>
    <li><a href="/health.html"><img src="/resources/img/nav/health.svg"><span>Health</span></a></li>
    <li><a href="/home.html"><img src="/resources/img/nav/home.svg"><span>Home & Garden</span></a></li>
    <li><a href="/jobs.html"><img src="/resources/img/nav/jobs.svg"><span>Jobs</span></a></li>
    <li><a href="/kids.html"><img src="/resources/img/nav/kids.svg"><span>Kids</span></a></li>
    <li><a href="/law.html"><img src="/resources/img/nav/law.svg"><span>Law</span></a></li>
    <li><a href="/lifestyle.html"><img src="/resources/img/nav/lifestyle.svg"><span>Lifestyle</span></a></li>
    <li><a href="/men.html"><img src="/resources/img/nav/men.svg"><span>Men</span></a></li>
    <li><a href="/movies.html"><img src="/resources/img/nav/movies.svg"><span>Movies</span></a></li>
    <li><a href="/music.html"><img src="/resources/img/nav/music.svg"><span>Music</span></a></li>
    <li><a href="/news.html"><img src="/resources/img/nav/news.svg"><span>News</span></a></li>
    <li><a href="/npo.html"><img src="/resources/img/nav/npo.svg"><span>NPO</span></a></li>
    <li><a href="/pets.html"><img src="/resources/img/nav/pets.svg"><span>Pets</span></a></li>
    <li><a href="/politics.html"><img src="/resources/img/nav/politics.svg"><span>Politics</span></a></li>
    <li><a href="/real_estate.html"><img src="/resources/img/nav/real_estate.svg"><span>Real Estate</span></a></li>
    <li><a href="/science.html"><img src="/resources/img/nav/science.svg"><span>Science</span></a></li>
    <li><a href="/seniors.html"><img src="/resources/img/nav/seniors.svg"><span>Seniors</span></a></li>
    <li><a href="/shopping.html"><img src="/resources/img/nav/shopping.svg"><span>Shopping</span></a></li>
    <li><a href="/sports.html"><img src="/resources/img/nav/sports.svg"><span>Sports</span></a></li>
    <li><a href="/style_fashion.html"><img src="/resources/img/nav/style_fashion.svg"><span>Style & Fashion</span></a></li>
    <li><a href="/teachers.html"><img src="/resources/img/nav/teachers.svg"><span>Teachers</span></a></li>
    <li><a href="/tech.html"><img src="/resources/img/nav/tech.svg"><span>Technology</span></a></li>
    <li><a href="/teens.html"><img src="/resources/img/nav/teens.svg"><span>Teens</span></a></li>
    <li><a href="/tips_tutorials.html"><img src="/resources/img/nav/tips_tutorials.svg"><span>Tips & Tutorials</span></a></li>
    <li><a href="/travel.html"><img src="/resources/img/nav/travel.svg"><span>Travel</span></a></li>
    <li><a href="/tools.html"><img src="/resources/img/nav/tools.svg"><span>Tools</span></a></li>
    <li><a href="/tv.html"><img src="/resources/img/nav/tv.svg"><span>TV</span></a></li>
    <li><a href="/women.html"><img src="/resources/img/nav/women.svg"><span>Women</span></a></li>
    <img class="arrow" src="/resources/img/nav/arrow.png">
  </ul>
</div>

<script>
  $('#moreMenuButton').on('mouseenter', function() {
      $(this).next().show()
  });
  $('#moreMenu').on('mouseleave', function() {
     $(this).children('ul').hide() ;
  });
</script>