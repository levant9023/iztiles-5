
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>iZSearch search engine | Investors</title>
    <link rel="shortcut icon" type="image/png" href="${baseURL}/resources/img/favicon.png">
    <link rel="stylesheet" type="text/css" href="${baseURL}/resources/js/production/bootstrap/css/bootstrap.css"/>
    <link type="text/css" rel="stylesheet" href="${baseURL}/resources/css/izsearch/subpages/investor_relations.css"/>
</head>
<body>
<header>
    <div class="container stn-header">
        <a href="/">
            <img src="${baseURL}/resources/img/logos/logo_search.svg" alt="" class="header-logo">
            <div class="header-name">Investors</div>
        </a>
    </div>
    <div class="hero-banner">
        <h1 class="container">Investors</h1>
    </div>
</header>
<section class="stn-content">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <b>
                    iZSearch Inc. owns and operates search engine platform. It offers technologies for enterprises such as iZCrawler, iZIndex, iZAds, and a set of other tools and algorithms. iZSearch offers Enterprise Level Search Solutions (ELSS) for corporate customers that helps in content discovery, big data analysis, data mining, and archiving. iZSearch customer areas include life sciences, pharmaceuticals, consumer electronics, insurance practices, legal practices, and military agencies where privacy is key.
                </b>
                <p>
                    Together with our members and shareholders, we are building an alternative Internet search that enhances privacy, providing convenient navigation features with minimal ads plus a clean and clear interface.
                </p>
                <p>
                    iZSearch provides services to search a wide variety of verticals. Enterprises can enhance their app or web site with advanced thematic search options and detailed insight into thematic news from over 40 areas of human interest. Businesses can get breaking, trending and popular news and other useful metadata with a single API call.
                </p>
                <p>
                    The company was incorporated in 2014 and is headquartered in Carlsbad, California. iZSearch Inc. is a Tech Cost Angels (TCA) portfolio company. iZSearch recently closed its seed funding round including investors from Tech Coast Angels (TCA) and Integrity Applications Incorporated (IAI).
                </p>
                <p>
                    For further enquiries about investor information please contact investorsATizsearch.com
                </p>
            </div>
        </div>
    </div>
</section>
<footer class="bottom-fix">
    <div class="col-md-12 text-center">
        <span style="font-size: 18px;">© iZSearch. Search it easy!</span>
    </div>
</footer>
</body>
</html>