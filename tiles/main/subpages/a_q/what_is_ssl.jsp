<%--
    Document   : more_features
    Created on : Oct 1, 2014, 11:15:04 PM
    Author     : efanchik
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="t" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="s" %>

<c:set var='title' value='${requestScope.title}' scope="request"/>

<div class="container-fluid">
  <div id="board" class="row">
    <div id="top-nav" class="row">
      <div class="col-md-4 text-left"></div>
      <div class="col-md-4 text-center"></div>
      <div class="col-md-4 text-right"></div>
    </div>
    <div id="logo" class="row">
      <div class="col-md-4 text-left"></div>
      <div class="col-md-4 text-center">
        <img src="${baseURL}/resources/img/logo.svg" alt="logo"/>
      </div>
      <div class="col-md-4 text-right"></div>
    </div>
    <div id="motto" class="row">
      <div class="col-md-4 text-left"></div>
      <div class="col-md-4 text-center">
        <h2>Questions</h2>
      </div>
      <div class="col-md-4 text-right"></div>
    </div>
  </div>
  <div id="arts" class="row">
    <div class="col-md-3"></div>
    <div class="col-md-6 center">
      <h3>What is a SSL/TLS Server Certificate?</h3>
      <p>Whenever you use your web browser to communicate securely over the Internet (i.e. for banking, private
        search through iZSearch, etc.) you are using a technology known as SSL (Secure SocketLayer) or TLS
        (Transport Layer Security). This technology serves TWO important purposes:</p>
      <ol>
        <li>
          <p>It provides end-to-end encryption of all data, so that your ISP, the NSA, a random hacker, etc.
          can't eavesdrop on the connection between you and the website you are communicating with (i.e. the
          bank, iZSearch, etc.).</p>
        </li>
        <li>
          <p>It provides authentication so you can confirm that the website you are communicating with is
            actually who it claims to be. That lets you confirm that a thief is not masquerading as your bank,
            or that a spy is not masquerading as iZSearch.

          <p>These two functions are both distinct and necessary. A connection to a trusted party is useless if
            it is not secure, and a secure connection to a party you can't trust is also useless.</p>

          <p>An SSL/TLS Server Certificate provides a website's authentication. You can think of it as being
            like a drivers license or a student ID card from a well-known university. It provides "official"
            identification for the website. If you need to prove your identity, you show your driver's license.
            If a website needs to do the same, it shows its SSL Server Certificate.</p>

          <p>Government ID might be issued by your state DMV, or a passport issued by the U.S. Federal
            government. "Internet ID" in the form of a SSL/TLS Server Certificate, is issued by a "Certificate
            Authority" (CA) -- a business entity entrusted with issuing SSL/TLS Server Certificates just like
            individual states are entrusted with issuing drivers' licenses. At last count, Mozilla's Firefox
            browser (for example) maintained a list of roughly 36 trusted CAs in total.</p>

          <p>If an SSL/TLS Server Certificate is not issued by one of these trusted CAs then your browser will
            typically either refuse the connection or issue numerous warnings you must override. That situation
            is like a bartender rejecting an ID card cooked up by someone's fraternity brother, also known as a
            "Fake ID."</p>

          <p>Wikipedia has comprehensive technical articles on these topics here:
            <a href="https://en.wikipedia.org/wiki/Certificate_authority">https://en.wikipedia.org/wiki/Certificate_authority</a><br>
            <a href="https://en.wikipedia.org/wiki/Transport_Layer_Security">https://en.wikipedia.org/wiki/Transport_Layer_Security</a>
          </p>

          <p>You can ask your web browser to show you the SSL/TLS Server Certificate for any secure website you
            visit.</p>

          <p>
            If you ask for iZSearch's (current as of 1/28/2015) SSL/TLS Server Certificate, you'll see:
          <table>
            <thead>
            <tr>
              <td>Issued To</td>
            </tr>
            </thead>
            <tbody>
            <tr>
              <td>Common Name (CN)</td>
              <td>*.iZSearch.com</td>
            </tr>
            <tr>
              <td>Organization (O)</td>
              <td>*.iZSearch.com</td>
            </tr>
            <tr>
              <td>Organizational Unit (OU)</td>
              <td>Domain Control Validated</td>
            </tr>
            <tr>
              <td>Serial Number</td>
              <td>27:9B:AA:D9:AD:ED:A9</td>
            </tr>
            </tbody>
          </table>
          </p>
          <p>
          <table>
            <thead>
            <tr>
              <td>Issued By</td>
            </tr>
            </thead>
            <tbody>
            <tr>
              <td>Common Name (CN)</td>
              <td>Go Daddy Secure Certification Authority</td>
            </tr>
            <tr>
              <td>Organization (O)</td>
              <td>GoDaddy.com, Inc.</td>
            </tr>
            <tr>
              <td>Organizational Unit (OU)</td>
              <td>http://certificates.godaddy.com/repository</td>
            </tr>
            </tbody>
          </table>
          </p>
          <p>
          <table>
            <thead>
            <tr>
              <td>Validity Period</td>
            </tr>
            </thead>
            <tbody>
            <tr>
              <td>Issued On</td>
              <td>3/16/11</td>
            </tr>
            <tr>
              <td>Expires On</td>
              <td>3/24/13</td>
            </tr>
            </tbody>
          </table>
          </p>
          <p>
          <table>
            <thead>
            <tr>
              <td>Fingerprints</td>
            </tr>
            </thead>
            <tbody>
            <tr>
              <td>SHA-256 Fingerprint</td>
              <td>34 78 92 BD 2C CF D3 46 81 C9 D0 24 72 E4 7A DA</td>
            </tr>
            <tr>
              <td>SHA-1 Fingerprint</td>
              <td>5C CF 20 7F 8C 8E 08 FC D8 D1 60 89 58 06 49 4C</td>
            </tr>
            </tbody>
          </table>
          </p>
        </li>
      </ol>
      <p>This information tells you that  iZSearch was issued an SSL/TLS Server Certificate by the "Go Daddy Secure Certification Authority." GoDaddy.com, Inc. is one of several different Certificate Authorities from whom a business or website such as iZSearch may purchase an SSL/TLS Server Certificate.</p>
      <p>Other major providers are GeoTrust, Comodo, Verisign, and Thawte. (See: <a href="http://www.whichssl.com/ssl-market-share.html">http://www.whichssl.com/ssl-market-share.html)</a></p>
    </div>
    <div class="col-md-3"></div>
  </div>

</div>
