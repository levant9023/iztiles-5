
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>iZSearch search engine | Ads for vertical</title>
    <link rel="shortcut icon" type="image/png" href="${baseURL}/resources/img/favicon.png">
    <link rel="stylesheet" type="text/css" href="${baseURL}/resources/js/production/bootstrap/css/bootstrap.css"/>
    <link rel="stylesheet" href="${baseURL}/resources/css/izsearch/subpages/advertise_vertical.css">
</head>
<body>
<header>
    <div class="container stn-header">
        <div class="row">
            <a class="logo" href="/">
                <img src="/resources/img/logos/logo_search.svg" alt="" class="header-logo">
                <div class="header-name">ADS FOR VERTICALS</div>
            </a>
        </div>
    </div>
    <div class="hero-banner">
        <h1 class="container">
            iZAds provides a possibility to advertise your business through the highly focused channels across a broad spectrum of areas.
        </h1>
    </div>
</header>
<section class="stn-content">
    <p class="category_title container">
        iZSearch Vertical/Thematic channels include: thematic news, topics, stories, articles, and reviews from over 40 areas of human interest including Top News and Blogs, Science and Technology, Politics, Sports, Food, Travel, Business, and Health, among many others.
        Promote your business through the Vertical/Thematic channels with the following types of Ads:
    </p>
    <div class="category-wrapper_1 container">
        <div class="col-md-5">
            <p class="category_item">
                Promoted Articles
            </p>
        </div>
        <div class="col-md-7">
            <div class="category_img"></div>
            <p class="category_descr">See your Ads/articles among latest articles on iZSearch Vertical pages.</p>
        </div>
    </div>
    <div class="category-wrapper_2 container">
        <div class="col-md-5">
            <p class="category_item">
                Vertical Search Snippets
            </p>
        </div>
        <div class="col-md-7">
            <div class="category_img"></div>
            <p class="category_descr">Your Ads/articles will be shown among Vertical search results.</p>
        </div>
    </div>
    <div class="category-wrapper_3 container">
        <div class="col-md-5">
            <p class="category_item">Add Your Company to iZSearch Listings</p>
        </div>
        <div class="col-md-7">
            <div class="category_img"></div>
            <p class="category_descr">You can add Your company to our listings at the top of the Thematic (e.g. Travel) page and see your data among latest vertical data and search results.</p>
        </div>
    </div>
    <div class="category-wrapper_4 container">
        <div class="col-md-5">
            <p class="category_item">Become a Sponsor of a Vertical Page</p>
        </div>
        <div class="col-md-7">
            <div class="category_img"></div>
            <p class="category_descr">Become a sponsor of our vertical page and your banner will always
                be at the top of the specific vertical page, and see your results among vertical search results.</p>
        </div>
    </div>
</section>
<footer class="bottom-fix">
    <div class="col-md-12 text-center">
        <span style="font-size: 18px;">© iZSearch. Search it easy!</span>
    </div>
</footer>
</body>
</html>
