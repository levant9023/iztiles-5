<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="/WEB-INF/tlds/izsearch.tld" prefix="iz" %>

<c:set var="sl_disabled" value="disabled"/>
<c:set var="motto" value="${requestScope['motto']}" scope="request"/>
<c:set var="sliders" value="${requestScope['events']}" scope="request"/>
<c:set var="inactive" value="${requestScope['inactive']}" scope="page"/>
<c:set var="all" value="${requestScope['all']}" scope="page"/>
<c:set var="tp" value="${requestScope['tp']}" scope="page"/>
<c:set var="evkey" value="${requestScope['ev_key']}"/>
<c:set var="evtitle" value="${requestScope['ev_title']}"/>
<c:set var="evcats" value="${requestScope['ev_cats']}"/>
<c:set var="popular" value="${requestScope['popular']}"/>
<c:set var="trending" value="${requestScope['trending_news']}"/>
<c:if test="${empty evkey}">
  <c:set var="evkey" value=""/>
</c:if>
<c:if test="${empty evtitle}">
  <c:set var="evtitle" value=""/>
</c:if>
<c:if test="${empty evcats}">
  <c:set var="evcats" value="[]"/>
</c:if>

<script type="text/javascript">
    var _slev = "${requestScope._slev}";
    var _slopen = "${requestScope._slopen}";
    var _pin = "${requestScope._pin}";
    var _tp = "${requestScope._tp}";
    var _evkey = "${evkey}";
    var _evtitle = "${evtitle}";
    var _evcats =  ${evcats};
</script>

<c:set var="active" value=""/>
<c:forEach var="el" items="${requestScope['active']}">
  <c:set var="active" value="${el}"/>
</c:forEach>

<!--<div class="container preloader-container">
  <div class="preloader-item">
    <div class="loader03"></div>
  </div>
</div>-->

<div id="main_sidebar">

  <span class="more_sidebar" style="display: none;">More</span>
  <div id="main_cats" class="sidebar_item">
    <!--<span class="item_name">Categories</span>-->
    <div id="list_toggle" class="dropdown">
      <button id="list_toggle_btn" class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
        <span class="standart_list glyphicon glyphicon-chevron-down"></span>
      </button>
      <ul class="dropdown-menu pull-right" aria-labelledby="moreMenuButton">
        <li id="popularity" class="active"><span>By popularity</span></li>
        <li id="alphabet"><span>Alphabetically</span></li>
      </ul>
    </div>
    <ul class="sd_cats trigger_cats"></ul>
  </div>
  <span class="more_sidebar">More</span>
</div>

<div id="sliders" class="${sl_disabled}">
<c:choose>

  <c:when test="${_ev}">
    <script>
        $(document).ready(function () {
            $('#ifb').animate("slow", function () {
                $(this).addClass("btn-hide");
            });
        });
    </script>
    <span id="sliders_toggle" title="Roll over"><i class="fa fa-chevron-down" aria-hidden="true"></i></span>
    <ul class="tab-class">
      <li id="mt2" class="mt-toggle ${motto['mt2']['status']}"><a href="${baseURL}/"><i class="fa fa-star" aria-hidden="true"></i>${motto['mt2']['title']}</a></li>
      <li id="trending" class="mt-toggle"><a href="${baseURL}/trending.html"><i class="fa fa-line-chart" aria-hidden="true"></i>Trending</a></li>
      <li id="popular" class="mt-toggle"><a href="${baseURL}/popular.html"><i class="fa fa-bullhorn" aria-hidden="true"></i>Popular</a></li>
    </ul>
    <c:if test="${not empty sliders}">
      <ul id="slick_tab" class="tabs themes inv" role="tablist">
        <c:forEach items="${sliders}" var="slider" varStatus="sls">
          <c:set var="status" value=""/>
          <c:if test="${sls.index eq 0}">
            <c:set var="status" value="active"/>
          </c:if>
          <c:set var="title" value="${iz:title_normalize(slider.key,1)}"/>
          <li role="presentation" class="${status}" style="width: ${iz:str_size_in_pixel(title, "Arial", 15, 1)}px">
            <a href="#${slider.key}" aria-controls="${slider.key}" role="tab" data-toggle="tab">${title}</a>
          </li>
        </c:forEach>
      </ul>
      <div class="tab-content">
        <c:forEach items="${sliders}" var="slider" varStatus="sls">
          <c:set var="status" value="active"/>
          <c:set var="title" value="${iz:title_normalize(slider.key,1)}"/>
          <div role="tabpanel" class="tab-pane ${status}" data-name="${slider.key}" id="${slider.key}">
            <div id="lfbadge${slider.key}" class="lfbadge">
              <a href="/${slider.key}.html?q=${active.value['key']}" data-toggle="tooltip" data-placement="right" title="${title}" data-original-title="${title}">
                <img class="badge_img" src="/resources/img/main/slick/${slider.key}.svg">
                <span class="category_name">${slider.key}</span>
              </a>
            </div>
            <section class="slick slick-slider">
              <c:set var="rss" value="${slider.value['rss']}"/>
              <c:if test="${not empty rss}">
                <c:forEach items="${rss}" var="elem" varStatus="esls">
                  <c:set var="url" value="${baseURL}/${elem['category']}/${elem['linkId']}/${elem['title4url']}.html"/>
                  <div class="slc">
                    <a class="plitka" href="${url}" style=" bottom : 0 ">
                      <c:set var="_img" value="${baseURL}/slides/${elem['cutImage']}"/>
                      <c:if test="${empty elem['cutImage'] || elem['cutImage'] eq 'none'}">
                        <c:set var="_img" value="${elem['image']}"/>
                      </c:if>
                      <img src="" data-lazy="${_img}">
                      <div class="snuggle">
                        <h6>${elem['title']}</h6>
                        <b class="domain">${elem['strippedUrl']}</b>
                        <b class="date_initialize">${elem['date']}</b>
                        <p class="description">${elem['description']}</p>
                      </div>
                    </a>
                  </div>
                </c:forEach>
              </c:if>
            </section>
          </div>
        </c:forEach>
      </div>
    </c:if>
  </c:when>

  <%-- INITIALIZATION OF SLIDERS FOR NON EVENTS MAIN PAGE --%>
  <c:otherwise>
    <%-- INITIALIZATION OF SLIDERS FOR NON EVENTS MAIN PAGE && ACTIVE PIN--%>
    <c:if test="${_pin eq true}">
      <span id="sliders_toggle" title="Roll over"><i class="fa fa-chevron-down" aria-hidden="true"></i></span>
      <c:if test="${not empty sliders}">
        <ul id="slick_tab" class="tabs themes inv" role="tablist">
          <c:forEach items="${sliders}" var="slider" varStatus="sls">
            <c:set var="status" value=""/>
            <c:if test="${sls.index eq 0}">
              <c:set var="status" value="active"/>
            </c:if>
            <c:set var="title" value="${iz:title_normalize(slider.key,1)}"/>
            <li role="presentation" class="${status}" title="${title}">
              <a href="#${slider.key}" aria-controls="${slider.key}" role="tab" data-toggle="tab">${title}</a>
            </li>
          </c:forEach>
        </ul>
        <span id="show_ev" class="show_ev_hide"><a>More>></a></span>
        <span id="hide_ev" class="show_ev_hide"><a>&lt;&lt;Less</a></span>
        <div class="tab-content grid-content">
          <div class="Popular_name"><i class="fa fa-bullhorn" aria-hidden="true"></i><span>Popular</span></div>
            <%@ include file="main_grid.jsp" %>
            <div role="tabpanel" class="tab-pane ${status}" data-name="${slider.key}" id="${slider.key}">

            </div>

        </div>
        <!--<div class="container preloader-container_2">
          <div class="preloader-item">
            <div class="loader03"></div>
          </div>
        </div>-->
      </c:if>
    </c:if>

    <%-- INITIALIZATION OF SLIDERS FOR NON EVENTS MAN PAGE && INACTIVE PIN--%>

    <c:if test="${_pin eq false}">
      <span id="sliders_toggle" title="Expand to full view"><i class="fa fa-chevron-up" aria-hidden="true"></i></span>
      <c:if test="${not empty sliders}">
        <ul id="slick_tab" class="tabs themes" role="tablist">
          <c:forEach items="${sliders}" var="slider" varStatus="sls">
            <c:set var="status" value=""/>
            <c:if test="${sls.index eq 0}">
              <c:set var="status" value="active"/>
            </c:if>
            <c:set var="title" value="${iz:title_normalize(slider.key,1)}"/>
              <li role="presentation" class="${status}" title="${title}">
                <a href="#${slider.key}" aria-controls="${slider.key}" role="tab" data-toggle="tab">${title}</a>
              </li>
          </c:forEach>
        </ul>
        <span id="show_ev"><a>More>></a></span>
        <span id="hide_ev"><a>&lt;&lt;Less</a></span>
        <div class="tab-content grid-content down">
          <div class="Popular_name"><i class="fa fa-bullhorn" aria-hidden="true"></i><span>Popular</span></div>
          <c:forEach items="${sliders}" var="slider" varStatus="sls">
            <c:set var="status" value=""/>
            <c:if test="${sls.index eq 0}">
              <c:set var="status" value="active"/>
            </c:if>
            <c:set var="title" value="${iz:title_normalize(slider.key,1)}"/>
            <div role="tabpanel" class="tab-pane ${status}" data-name="${slider.key}" id="${slider.key}">
              <div id="lfbadge${slider.key}" class="lfbadge">
                <a href="/${slider.key}.html" data-toggle="tooltip" data-placement="right" title="${title}" data-original-title="${title}">
                  <img class="badge_img" src="resources/img/main/slick/${slider.key}.svg">
                  <span class="category_name">${slider.key}</span>
                </a>
              </div>
              <section class="grid_layout ${slider.key}">
                <c:if test="${sls.index eq 0}">
                  <c:set var="rss" value="${slider.value['rss']}"/>
                  <c:if test="${not empty rss}">
                    <c:forEach items="${trending}" var="elem" varStatus="esls">
                      <c:set var="url" value="${baseURL}/${elem['category']}/${elem['linkId']}/${elem['title4url']}.html"/>
                      <c:set var="_img" value="${baseURL}/slides/${elem['cutImage']}"/>
                      <c:set var="_fullImg" value="${elem['image']}" />
                      <c:if test="${empty elem['cutImage'] || elem['cutImage'] eq 'none'}">
                        <c:set var="_img" value="${elem['image']}"/>
                      </c:if>
                      <c:if test="${esls.index < 7}">
                        <div class="grid_item bottom rows">
                          <a href="${url}">
                            <img src="${_img}">
                            <div class="snuggle">
                              <h6>${elem['title']}</h6>
                              <b class="domain">${elem['strippedUrl']}</b>
                              <b class="date_initialize">${elem['date']}</b>
                              <p class="description">${elem['description']}</p>
                            </div>
                          </a>
                        </div>
                      </c:if>
                    </c:forEach>
                  </c:if>
                </c:if>
              </section>
              <div class="popular-content">
                <section class="popular_cont default_pupular ${slider.key}"></section>
                <section class="popular_cont more_popular1 ${slider.key}"></section>
                <section class="popular_cont more_popular2 ${slider.key}"></section>
              </div>
            </div>
          </c:forEach>
        </div>
      </c:if>
    </c:if>
  </c:otherwise>
</c:choose>
</div>
