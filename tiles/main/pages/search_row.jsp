<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<c:set var="all" value="${requestScope['all']}" scope="page"/>
<c:set var="_ev" value="${requestScope['_slev']}" scope="page"/>
<c:set var="q" value="${requestScope['q']}" scope="page"/>
<c:set var="b" value="${requestScope['b']}" scope="page"/>
<c:set var="_tp" value="${requestScope['_tp']}" scope="page"/>

<c:set var="lcount" value="${fn:length(all)}" />

<c:set var="mvisb" value=""/>
<c:set var="tvisb" value=""/>
<c:set var="isev" value=""/>


<c:if test="${_ev eq 'true'}">
  <c:set var="mvisb" value="hidden"/>
  <c:set var="isev" value="true"/>
</c:if>

<c:if test="${_ev eq 'false'}">
  <c:set var="tvisb" value="hidden"/>
  <c:set var="isev" value="false"/>
  <c:if test="${_pin eq 'true'}">
    <c:set var="tvisb" value=""/>
    <c:set var="mvisb" value="hidden"/>
  </c:if>
</c:if>

<c:if test="${not empty _tp}">
  <c:set var="mvisb" value="hidden"/>
  <c:set var="isev" value="false"/>
</c:if>

<%--
<c:choose>
  <c:when test="${not empty q}">
    <c:set var="query" value="${q}"/>
  </c:when>
  <c:otherwise>
    <c:if test="${not empty b}">
      <c:set var="query" value="${b}"/>
    </c:if>
  </c:otherwise>
</c:choose>--%>

<div id="search-row-slicked" class="m02 ${tvisb}">
  <div class="logo-block">
    <div class="logo">
      <a href="${baseURL}/">
        <img src="${baseURL}/resources/img/logo_search_line.svg" alt="izsearch logotype">
      </a>
    </div>
  </div>
  <div class="search-block">
    <div id="moreMenu" class="dropdown">
      <button id="moreMenuButton" class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" title="Discover business news, technical reviews, recipes, travel, style and other ideas to try.">
        <img src="${baseURL}/resources/img/nav/menu.png" class="icon-inert">
      </button>
      <ul class="dropdown-menu" aria-labelledby="moreMenuButton" style="right: -335px;">
        <span class="moreMenu_title">Explore Thematic Pages</span>
        <li><a href="/art.html"><img src="/resources/img/nav/art.svg"><span>Art</span></a></li>
        <li><a href="/blogs_magazines.html"><img src="/resources/img/nav/blogs_magazines.svg"><span>Blogs &amp; Magazines</span></a></li>
        <li><a href="/books.html"><img src="/resources/img/nav/books.svg"><span>Books</span></a></li>
        <li><a href="/business.html"><img src="/resources/img/nav/business.svg"><span>Business</span></a></li>
        <li><a href="/cars.html"><img src="/resources/img/nav/cars.svg"><span>Cars</span></a></li>
        <li><a href="/celebrity.html"><img src="/resources/img/nav/celebrity.svg"><span>Celebrity</span></a></li>
        <li><a href="/craft.html"><img src="/resources/img/nav/craft.svg"><span>Craft</span></a></li>
        <li><a href="/design.html"><img src="/resources/img/nav/design.svg"><span>Design</span></a></li>
        <li><a href="/education.html"><img src="/resources/img/nav/education.svg"><span>Education</span></a></li>
        <li><a href="/family.html"><img src="/resources/img/nav/family.svg"><span>Family</span></a></li>
        <li><a href="/food_recipes.html"><img src="/resources/img/nav/food_recipes.svg"><span>Food &amp; Recipes</span></a></li>
        <li><a href="/fun.html"><img src="/resources/img/nav/fun.svg"><span>Fun</span></a></li>
        <li><a href="/gifts.html"><img src="/resources/img/nav/gifts.svg"><span>Gifts</span></a></li>
        <li><a href="/health.html"><img src="/resources/img/nav/health.svg"><span>Health</span></a></li>
        <li><a href="/home.html"><img src="/resources/img/nav/home.svg"><span>Home &amp; Garden</span></a></li>
        <li><a href="/jobs.html"><img src="/resources/img/nav/jobs.svg"><span>Jobs</span></a></li>
        <li><a href="/kids.html"><img src="/resources/img/nav/kids.svg"><span>Kids</span></a></li>
        <li><a href="/law.html"><img src="/resources/img/nav/law.svg"><span>Law</span></a></li>
        <li><a href="/lifestyle.html"><img src="/resources/img/nav/lifestyle.svg"><span>Lifestyle</span></a></li>
        <li><a href="/men.html"><img src="/resources/img/nav/men.svg"><span>Men</span></a></li>
        <li><a href="/movies.html"><img src="/resources/img/nav/movies.svg"><span>Movies</span></a></li>
        <li><a href="/music.html"><img src="/resources/img/nav/music.svg"><span>Music</span></a></li>
        <li><a href="/news.html"><img src="/resources/img/nav/news.svg"><span>News</span></a></li>
        <li><a href="/npo.html"><img src="/resources/img/nav/npo.svg"><span>NPO</span></a></li>
        <li><a href="/pets.html"><img src="/resources/img/nav/pets.svg"><span>Pets</span></a></li>
        <li><a href="/politics.html"><img src="/resources/img/nav/politics.svg"><span>Politics</span></a></li>
        <li><a href="/real_estate.html"><img src="/resources/img/nav/real_estate.svg"><span>Real Estate</span></a></li>
        <li><a href="/science.html"><img src="/resources/img/nav/science.svg"><span>Science</span></a></li>
        <li><a href="/seniors.html"><img src="/resources/img/nav/seniors.svg"><span>Seniors</span></a></li>
        <li><a href="/shopping.html"><img src="/resources/img/nav/shopping.svg"><span>Shopping</span></a></li>
        <li><a href="/sports.html"><img src="/resources/img/nav/sports.svg"><span>Sports</span></a></li>
        <li><a href="/style_fashion.html"><img src="/resources/img/nav/style_fashion.svg"><span>Style &amp; Fashion</span></a></li>
        <li><a href="/teachers.html"><img src="/resources/img/nav/teachers.svg"><span>Teachers</span></a></li>
        <li><a href="/tech.html"><img src="/resources/img/nav/tech.svg"><span>Technology</span></a></li>
        <li><a href="/teens.html"><img src="/resources/img/nav/teens.svg"><span>Teens</span></a></li>
        <li><a href="/tips_tutorials.html"><img src="/resources/img/nav/tips_tutorials.svg"><span>Tips &amp; Tutorials</span></a></li>
        <li><a href="/travel.html"><img src="/resources/img/nav/travel.svg"><span>Travel</span></a></li>
        <li><a href="/tools.html"><img src="/resources/img/nav/tools.svg"><span>Tools</span></a></li>
        <li><a href="/tv.html"><img src="/resources/img/nav/tv.svg"><span>TV</span></a></li>
        <li><a href="/women.html"><img src="/resources/img/nav/women.svg"><span>Women</span></a></li>
        <img class="arrow" style="right: 351px;" src="/resources/img/nav/arrow.png">
      </ul>
    </div>
    <form id="slicked-search-form" role="search" action="${baseURL}/search" method="GET" target="_self" accept-charset="UTF-8">
      <input id="slicked-input-search" type="text" placeholder="Search it easy..." class="form-control" name="q" value="<c:out value="${q}" default=""/>" autofocus
             autocomplete="off">
      <button id="btn-search" type="submit" class="input-inside">
        <!--<img src="${baseURL}/resources/img/lence_search.png" alt="lence">-->
        <i class="glyphicon glyphicon-search"></i>
      </button>
      <div id="slicked-autosuggest" class="form-group">
        <ul id="slicked-autocomplete" class="autocomplete"></ul>
      </div>
    </form>
  </div>
</div>
<div id="search-row" class="m01 ${mvisb}">
  <a href="${baseURL}/">
    <img class="logo" src="${baseURL}/resources/img/logo_search.svg" alt="izsearch logotype">
  </a>
  <div id="search-panel">
    <div id="moreMenu1" class="dropdown">
      <button id="moreMenuButton1" class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" title="Discover business news, technical reviews, recipes, travel, style and other ideas to try.">
        <img src="${baseURL}/resources/img/nav/menu.png" class="icon-inert">
      </button>
      <ul class="dropdown-menu" aria-labelledby="moreMenuButton" style="right: -277px;">
        <span class="moreMenu_title">Explore Thematic Pages</span>
        <li><a href="/art.html"><img src="/resources/img/nav/art.svg"><span>Art</span></a></li>
        <li><a href="/blogs_magazines.html"><img src="/resources/img/nav/blogs_magazines.svg"><span>Blogs &amp; Magazines</span></a></li>
        <li><a href="/books.html"><img src="/resources/img/nav/books.svg"><span>Books</span></a></li>
        <li><a href="/business.html"><img src="/resources/img/nav/business.svg"><span>Business</span></a></li>
        <li><a href="/cars.html"><img src="/resources/img/nav/cars.svg"><span>Cars</span></a></li>
        <li><a href="/celebrity.html"><img src="/resources/img/nav/celebrity.svg"><span>Celebrity</span></a></li>
        <li><a href="/craft.html"><img src="/resources/img/nav/craft.svg"><span>Craft</span></a></li>
        <li><a href="/design.html"><img src="/resources/img/nav/design.svg"><span>Design</span></a></li>
        <li><a href="/education.html"><img src="/resources/img/nav/education.svg"><span>Education</span></a></li>
        <li><a href="/family.html"><img src="/resources/img/nav/family.svg"><span>Family</span></a></li>
        <li><a href="/food_recipes.html"><img src="/resources/img/nav/food_recipes.svg"><span>Food &amp; Recipes</span></a></li>
        <li><a href="/fun.html"><img src="/resources/img/nav/fun.svg"><span>Fun</span></a></li>
        <li><a href="/gifts.html"><img src="/resources/img/nav/gifts.svg"><span>Gifts</span></a></li>
        <li><a href="/health.html"><img src="/resources/img/nav/health.svg"><span>Health</span></a></li>
        <li><a href="/home.html"><img src="/resources/img/nav/home.svg"><span>Home &amp; Garden</span></a></li>
        <li><a href="/jobs.html"><img src="/resources/img/nav/jobs.svg"><span>Jobs</span></a></li>
        <li><a href="/kids.html"><img src="/resources/img/nav/kids.svg"><span>Kids</span></a></li>
        <li><a href="/law.html"><img src="/resources/img/nav/law.svg"><span>Law</span></a></li>
        <li><a href="/lifestyle.html"><img src="/resources/img/nav/lifestyle.svg"><span>Lifestyle</span></a></li>
        <li><a href="/men.html"><img src="/resources/img/nav/men.svg"><span>Men</span></a></li>
        <li><a href="/movies.html"><img src="/resources/img/nav/movies.svg"><span>Movies</span></a></li>
        <li><a href="/music.html"><img src="/resources/img/nav/music.svg"><span>Music</span></a></li>
        <li><a href="/news.html"><img src="/resources/img/nav/news.svg"><span>News</span></a></li>
        <li><a href="/npo.html"><img src="/resources/img/nav/npo.svg"><span>NPO</span></a></li>
        <li><a href="/pets.html"><img src="/resources/img/nav/pets.svg"><span>Pets</span></a></li>
        <li><a href="/politics.html"><img src="/resources/img/nav/politics.svg"><span>Politics</span></a></li>
        <li><a href="/real_estate.html"><img src="/resources/img/nav/real_estate.svg"><span>Real Estate</span></a></li>
        <li><a href="/science.html"><img src="/resources/img/nav/science.svg"><span>Science</span></a></li>
        <li><a href="/seniors.html"><img src="/resources/img/nav/seniors.svg"><span>Seniors</span></a></li>
        <li><a href="/shopping.html"><img src="/resources/img/nav/shopping.svg"><span>Shopping</span></a></li>
        <li><a href="/sports.html"><img src="/resources/img/nav/sports.svg"><span>Sports</span></a></li>
        <li><a href="/style_fashion.html"><img src="/resources/img/nav/style_fashion.svg"><span>Style &amp; Fashion</span></a></li>
        <li><a href="/teachers.html"><img src="/resources/img/nav/teachers.svg"><span>Teachers</span></a></li>
        <li><a href="/tech.html"><img src="/resources/img/nav/tech.svg"><span>Technology</span></a></li>
        <li><a href="/teens.html"><img src="/resources/img/nav/teens.svg"><span>Teens</span></a></li>
        <li><a href="/tips_tutorials.html"><img src="/resources/img/nav/tips_tutorials.svg"><span>Tips &amp; Tutorials</span></a></li>
        <li><a href="/travel.html"><img src="/resources/img/nav/travel.svg"><span>Travel</span></a></li>
        <li><a href="/tools.html"><img src="/resources/img/nav/tools.svg"><span>Tools</span></a></li>
        <li><a href="/tv.html"><img src="/resources/img/nav/tv.svg"><span>TV</span></a></li>
        <li><a href="/women.html"><img src="/resources/img/nav/women.svg"><span>Women</span></a></li>
        <img class="arrow" style="right: 294px;" src="/resources/img/nav/arrow.png">
      </ul>
    </div>
    <form id="search-form" action="${baseURL}/search" method="GET" target="_self" accept-charset="UTF-8">
      <div class="input-group">
        <input id="input-search" type="text" class="form-control flat input-lg" name="q" value="<c:out value="${requestScope.query}" default=""/>" autofocus autocomplete="off">
        <button id="btn-lence" class="input-inside" type="submit"><i class="glyphicon glyphicon-search"></i></button>
        <div id="autosuggest" class="form-group">
          <ul id="autocomplete" class="autocomplete"></ul>
        </div>
      </div>
    </form>
  </div>
  <!-- Dinamicescaia izmenenie nadpisi pod strakoi zaprosa -->
  <div class="event-motto"></div>
  <section id="carousel">
    <div class="carousel slide" id="fade-quote-carousel" data-ride="carousel" data-interval="5000">

      <c:set var="mactiv" value=""/>
      <c:if test="${isev eq 'false'}">
        <c:set var="mactiv" value="active"/>
      </c:if>

      <!-- Carousel indicators -->
      <ol class="carousel-indicators">
        <li data-target="#fade-quote-carousel" data-slide-to="0"></li>
        <li data-target="#fade-quote-carousel" data-slide-to="1"></li>
      </ol>

      <!-- Carousel items -->
      <div class="carousel-inner">
        <div class="item active">
          <blockquote>
            <div class="event-name">
              <div id="elements">
                <span>Super-quick <a href="${baseUrl}/food_recipes.html?q=quick soup">soups</a></span>
                <span>Healthier <a href="${baseUrl}/food_recipes.html?q=casserole">casseroles</a></span>
                <span>Use Your <a href="${baseUrl}/food_recipes.html?q=Noodle">Noodle</a></span>
                <span><a href="${baseUrl}/food_recipes.html?q=Make ahead">Make-ahead</a> meals</span>
                <span>Ready, <a href="${baseUrl}/food_recipes.html?q=spaghetti">Spaghetti</a>, Go!</span>
                <span><a href="${baseUrl}/food_recipes.html?q=high protein">High protein</a> meatless recipes</span>
                <span>Easy Spooky <a href="${baseUrl}/food_recipes.html?q=Snacks">Snacks</a></span>
                <span><a href="${baseUrl}/food_recipes.html?q=corn">A-maize-ing</a> recipes</span>
                <span>Killer <a href="${baseUrl}/food_recipes.html?q=Kale">Kale</a> Dishes</span>
                <span>Fresh <a href="${baseUrl}/food_recipes.html?q=seafood">Catch</a></span>
                <span><a href="${baseUrl}/food_recipes.html?q=seasonal">Seasonal Food</a></span>
                <span>Veni, <a href="${baseUrl}/food_recipes.html?q=veggie">veggie</a>, vici!</span>
                <span>Sip your <a href="${baseUrl}/food_recipes.html?q=soup">soup</a></span>
                <span>Amen to <a href="${baseUrl}/food_recipes.html?q=ramen">ramen</a>!</span>
                <span><a href="${baseUrl}/food_recipes.html?q=Meatless">Meatless meals</a></span>
                <span>Feeling <a href="${baseUrl}/food_recipes.html?q=blood+orange">bloody</a>?</span>
                <span>Babushka, balalaika, <a href="${baseUrl}/food_recipes.html?q=pelmeni">pelmeni</a></span>
                <span>Healthy <a href="${baseUrl}/food_recipes.html?q=avocado">Avocados</a></span>
                <span>Smoooooth <a href="${baseUrl}/food_recipes.html?q=smoothie">smoothies</a></span>
                <span>Going back to the <a href="${baseUrl}/food_recipes.html?q=Root+Vegetables">roots</a></span>
                <span>Trending in <a href="${baseUrl}/cars.html">cars and motorcycles</a> this week</span>
                <span><a href="${baseUrl}/style_fashion.html?q=surf">Surf culture</a>, <a href="${baseUrl}/style_fashion.html?q=tennis">tennis clothes</a> and other fashion trends</span>
                <span><a href="${baseUrl}/cars.html?q=Classic">Classic cars</a>, <a href="${baseUrl}/cars.html?q=Volkswagen camper">Volkswagen campers</a> and other trends in cars</span>
                <span><a href="${baseUrl}/travel.html?q=surfing">Surfing</a>, <a href="${baseUrl}/travel.html?q=campers">Campers</a>, <a href="${baseUrl}/travel.html?q=Luxury hotels">Luxury hotels</a> and other travel trending topics</span>
                <span><a href="${baseUrl}/travel.html?q=Iceland">Iceland</a>, <a href="${baseUrl}/travel.html?q=New Zealand">New Zealand</a>, <a href="${baseUrl}/travel.html?q=Puerto Rico">Puero Rico</a> and other trending travel destinations</span>
                <span><a href="${baseUrl}/style_fashion.html?q=Vans">Vans</a>, <a href="${baseUrl}/style_fashion.html?q=Gucci">Gucci</a> and other brands trending this week</span>
                <span><a href="${baseUrl}/design.html?q=Home">Home</a> and <a href="${baseUrl}/design.html?q=Industrial">Industrial</a> design and other design trends</span>
                <span><a href="${baseUrl}/food_recipes.html?q=Russian">Russian food</a>, <a href="${baseUrl}/travel.html?q=Italy">Italy travel</a>  and other topics picked for you</span>
                <span><a href="${baseUrl}/food_recipes.html?q=Healthy">Healthy food</a>, <a href="${baseUrl}/food_recipes.html?q=Salsa">Salsa</a> and other trends in <a href="${baseUrl}/food_recipes.html">Food and drinks</a> </span>


              </div>
            </div>
          </blockquote>
        </div>
        <div class="item">
          <blockquote>
            <div class="event-name">
              <span>Promote your business with <a href="${baseURL}/brand/intro.html">iZBrands</a> and <a href="${baseURL}/advertise.html">iZAds</a></span>
            </div>
          </blockquote>
        </div>
      </div>
    </div>
  </section>
</div>

<script type="text/javascript" src="${baseURL}/resources/js/production/plugins/social-share/jquery.share.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#btn-social').mouseenter(function (e) {
            $('.social').toggleClass('opened');
        });

        $('#social').share({
            networks: ['facebook', 'twitter', 'googleplus'],
            theme: 'square',
            urlToShare: 'http://izsearch.com/about.html'
        });

        var spans = $('#elements span');
        var span = spans.eq(Math.floor(Math.random() * spans.length));
        span.show();
        setTimeout(function doSomething() {
            console.log("10 seconds");
            spans.hide();
            span = spans.eq(Math.floor(Math.random() * spans.length));
            span.show();
            setTimeout(doSomething, 8000);
        }, 8000);

    });
</script>