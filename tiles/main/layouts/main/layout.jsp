<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<c:set var="req" value="${pageContext.request}" />
<c:set var="domain" value="${req.scheme}://${req.serverName}:${req.serverPort}"/>
<c:set var="queryString" value="${req.queryString}"/>
<c:set var="baseURL" value="${domain}${req.contextPath}${queryString}"/>
<c:set var="lang" value="${fn:substring(requestScope.locale,0, 2)}"/>

<c:set var="isevent" value="${cookie._slev.value}" scope="request"/>
<c:set var="_ev" value="${requestScope['_slev']}" scope="request"/>
<c:set var="_pin" value="${requestScope['_pin']}" scope="request"/>

<c:set var="wh" value="sl-top" />
<c:if test="${_ev eq 'false'}">
	<c:set var="wh" value='sl-bottom' />
</c:if>
<c:if test="${_pin eq 'true'}">
	<c:set var="wh" value='sl-top' />
</c:if>

<!DOCTYPE html>
<html lang='<c:out value="${lang}" default="en"/>'>
	<tiles:insertAttribute name="header"/>
	<body>
		<div id="main-wrapper" class="wrapper ${wh}">
			<tiles:insertAttribute name="content"/>
		</div>
		<tiles:insertAttribute name="footer"/>
	</body>
</html>
