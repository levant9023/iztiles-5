<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<c:set var="meta_title" value="${requestScope.meta_title}" />

<head>
	<base href="${baseURL}/">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>iZSearch search engine | <c:out value="${meta_title} Desktop version." default="iZSearch search engine. Desktop version."/></title>

	<meta name="description" content="iZSearch - private search engine that finds and returns relevant web sites, images, videos across many categories. Get a realtime feeds of breaking news, business and technology blogs, fun stories and travel ideas, discover recipes, home and shopping ideas, fashion inspiration and other ideas to try.">
	<meta name="keywords" content="izsearch, izsearch home, izsearch home page, izsearch search, izsearch web, news, blog, forum, health, sports, travel, tech, entertainment, video, image">

	<link rel="search" type="application/opensearchdescription+xml" title="iZSearch" href="${baseURL}/provider.xml">
	<link rel="shortcut icon" type="image/png" href="${baseURL}/resources/img/favicon.png">

	<link rel="stylesheet" type="text/css" href="${baseURL}/resources/js/production/bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="${baseURL}/resources/js/production/plugins/social-share/jquery.share.css">
	<link rel="stylesheet" type="text/css" href="${baseURL}/resources/font-awesome-4.3.0/css/font-awesome.min.css">
	<!-- Slick CSS -->
	<link rel="stylesheet" type="text/css" href="${baseURL}/resources/js/production/plugins/slick-1.5.9/slick.css">
	<link rel="stylesheet" type="text/css" href="${baseURL}/resources/js/production/plugins/slick-1.5.9/slick-theme.css">
	<link rel="stylesheet" type="text/css" href="${baseURL}/resources/css/izsearch/animate.css">
	<c:if test="${(_pin eq false)}">
		<link rel="stylesheet" type="text/css" href="${baseURL}/resources/css/izsearch/main.css">
	</c:if>
	<c:if test="${(_pin eq true)}">
		<link rel="stylesheet" type="text/css" href="${baseURL}/resources/css/izsearch/main_pinned.css">
	</c:if>
	<link rel="stylesheet" type="text/css" href="${baseURL}/resources/css/izsearch/main_grid.css">


	<script type="text/javascript" src="${baseURL}/resources/js/production/bootstrap/js/jquery-1.11.1.min.js"></script>
	<script type="text/javascript" src="${baseURL}/resources/js/production/bootstrap/js/bootstrap.js"></script>
	<script type="text/javascript" src="${baseURL}/resources/js/production/bootstrap/js/can.min.js"></script>
	<!-- Slick JS -->
	<script type="text/javascript" src="${baseURL}/resources/js/production/modules/js.cookie.js"></script>
	<script type="text/javascript" src="${baseURL}/resources/js/production/plugins/slick-1.5.9/slick.js"></script>
	<script type="text/javascript" src="${baseURL}/resources/js/production/mvc/classes.js"></script>
	<script type="text/javascript" src="${baseURL}/resources/js/production/mvc/main_app.js"></script>
	<script type="text/javascript" src="${baseURL}/resources/js/production/lazyload/jquery.lazyload.min.js"></script>
	<script type="text/javascript" data-main="${baseURL}/resources/js/production/main_page.js"
					src="${baseURL}/resources/js/production/require.js"></script>

	<script>
      (function (i, s, o, g, r, a, m) {
          i['GoogleAnalyticsObject'] = r;
          i[r] = i[r] || function () {
              (i[r].q = i[r].q || []).push(arguments)
          }, i[r].l = 1 * new Date();
          a = s.createElement(o),
                  m = s.getElementsByTagName(o)[0];
          a.async = 1;
          a.src = g;
          m.parentNode.insertBefore(a, m)
      })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
      ga('create', 'UA-58665066-1', 'auto');
      ga('require', 'linkid', 'linkid.js');
      ga('send', 'pageview');
      setTimeout("ga('send', 'event', 'Pageview', 'Page visit 15 sec. or more')", 1500);
	</script>
<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "ItemList",
  "itemListElement": [
    {
      "@type": "ListItem",
      "position": 1,
      "url": "https://izsearch.com/about.html"
 	},
   {
      "@type": "ListItem",
      "position": 2,
      "url": "https://blog.izsearch.com"
    },
    {
      "@type": "ListItem",
      "position": 3,
      "url": "https://izsearch.com/api.html.html"
      },
		{
      "@type": "ListItem",
      "position": 4,
      "url": "https://izsearch.com/izbrands.html"
      },
		{
      "@type": "ListItem",
      "position": 5,
      "url": "https://izsearch.com/advertise.html"
      },
		{
      "@type": "ListItem",
      "position": 6,
      "url": "https://izsearch.com/investor_relations.html"
      }
    ]
  }
</script>
</head>
