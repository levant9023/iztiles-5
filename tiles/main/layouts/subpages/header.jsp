<%-- 
    Document   : header
    Created on : Oct 1, 2014, 11:15:04 PM
    Author     : efanchik
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<c:set var="meta_title" value="${requestScope.meta_title}" />

<head>
  <base href="${baseURL}/">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>iZSearch search engine | <c:out value="${meta_title}" default="iZSearch search engine."/></title>
  <meta name="description" content="iZSearch has everything you expect a search engine to have, including images, videos, news and places, popular verticals like Sports, Health, Science, Travel, Music, Blogs, Food, Politics, Tech and other. And all these while respecting your privacy.">
  <meta name="keywords"
        content="izsearch, izsearch home, izsearch home page, izsearch search, izsearch web, news, blog, forum, health, sports, travel, tech, entertainment, video, image">
  <link rel="shortcut icon" type="image/png" href="${baseURL}/resources/img/favicon.png"/>
  <link rel="stylesheet" type="text/css" href="${baseURL}/resources/js/production/bootstrap/css/bootstrap.css"/>
  <link rel="stylesheet" type="text/css" href="${baseURL}/resources/css/izsearch/subpages.css"/>
  <script type="text/javascript" src="${baseURL}/resources/js/production/bootstrap/js/jquery-1.11.1.min.js"></script>
  <script type="text/javascript" src="${baseURL}/resources/js/production/bootstrap/js/bootstrap.js"></script>
  <script type="text/javascript" src="${baseURL}/resources/js/production/bootstrap/js/can.min.js"></script>
  <script type="text/javascript"
          data-main="${baseURL}/resources/js/production/main_page.js"
          src="${baseURL}/resources/js/production/require.js">
  </script>

  <!-- Google Analytics -->
  <script type="text/javascript">
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
              m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
      ga('create', 'UA-58665066-1', 'auto');
      ga('send', 'pageview');
      setTimeout("ga('send', 'event', 'Pageview', 'Page visit 15 sec. or more')", 1500);
  </script>
</head>