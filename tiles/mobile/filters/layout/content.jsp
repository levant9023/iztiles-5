<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="path" value="${requestScope.wpath}" />
<c:set var="cl" value="" />
<c:if test="${not empty path}">
	<c:choose>
		<c:when test="${fn:contains(path, 'images.html')}">
			<c:set var="cl" value="filter-images" />
			<c:set var="nbf" value="false"/>
		</c:when>
		<c:when test="${fn:contains(path, 'video.html')}">
			<c:set var="cl" value="filter-videos" />
			<c:set var="nbf" value="false"/>
		</c:when>
		<c:when test="${fn:contains(path, 'buttons.html')}">
			<c:set var="cl" value="filter-buttons" />
			<c:set var="nbf" value="false"/>
		</c:when>
		<c:otherwise>
			<c:set var="cl" value="filter-text" />
			<c:set var="nbf" value="true"/>
		</c:otherwise>
	</c:choose>
</c:if>

<div id="content">
	<div id="amazon"><div class="izslik"></div></div>
	<div id="filter-images" class="content"><tiles:insertAttribute name="right_block"/></div>
	<div id="filter-text" class="content <c:out value="${cl}" default="" />">
		<div id="related-text"></div>
		<div id="sec01" class="sec"></div>
		<div id="article-delimeter"></div>
		<div id="treading-popular-wrapper">
			<div class="ct_breaking"></div>
			<section class="latest-blog-posts bg-white pt60 pb60">
				<div id="breaking" class="container-fluid owl-carousel2 faves-list owl-theme" style="display:none;">
					<div id="owl-demo-2" class="faves-list owl-carousel2 owl-theme"></div>
				</div>
			</section>
			<div class="ct_trending"></div>
			<section class="latest-blog-posts bg-white pt60 pb60">
				<div id="trending" class="container-fluid owl-carousel3 faves-list owl-theme" style="display:none;">
					<div id="owl-demo-2" class="faves-list owl-carousel3 owl-theme"></div>
				</div>
			</section>
			<div class="ct_popular"></div>
			<section class="latest-blog-posts bg-white pt60 pb60">
				<div id="popular" class="container-fluid faves-list owl-carousel1 owl-theme" style="display:none;">
					<div id="owl-demo-2" class="faves-list owl-carousel1 owl-theme"></div>
				</div>
			</section>
		</div>
		<div id="sec02" class="sec"></div>
	</div>
	<tiles:insertAttribute name="sections"/>
</div>
<a id="top" href="javascript://"><i id="soc_icon" class="glyphicon glyphicon-chevron-up"></i></a>
<span class="socialShare"> <!-- The share buttons will be inserted here --> </span>
<script type="text/javascript" src="${baseURL}/resources/js/production/mobile/js/mvc/controls/filters/trending_popular.js"></script>