<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/izsearch.tld" prefix="iz" %>

<c:set var="top_news" value="${requestScope.topSearch}"/>
<c:set var="debug" value="${requestScope.debug}"/>

<div id="top-news1" class="block">
	<c:forEach var="snippet" items="${top_news}" varStatus="newstatus" end="9">
		<c:set var="item" value="${snippet.mainLink}"/>
		<c:if test="${not empty item}">
			<article class="art">
				<c:set var="title" value="${iz:clear_descr(iz:text_normalize(fn:trim(item.title)))}"/>
				<c:set var="full_title" value="${title}"/>
				<c:set var='title' value='${iz:mark_words(title, query)}'/>
				<c:url var="uri" value="${item.url}"/>
				<c:if test="${empty title}">
					<c:set var="title" value="${iz:clear_www(iz:get_domain(uri))}"/>
					<c:set var="title" value="${iz:cap_word(title)}"/>
				</c:if>
				<c:set var="descr" value="${fn:trim(item.description)}"/>
				<c:if test="${empty descr}">
					<c:set var="descr" value="${fn:trim(item.textDescription)}"/>
				</c:if>
				<c:if test="${empty descr}">
					<c:set var="descr" value="${title}"/>
				</c:if>
				<c:set var="descr" value="${iz:clear_descr(iz:text_normalize(descr))}"/>
				<c:set var="title_formated" value="<b>${iz:html_quotes(title)}</b><br>${iz:html_quotes(descr)}"/>
				<c:set var="desc" value="${iz:strip_str_pxl(descr, ' ', 'Arial', 13, 980)}"/>
				<c:set var="desc" value='${iz:mark_words(desc, query)}'/>
				<h3 class="art-news-title" data-toggle="art-title-popup" data-title="${title_formated}" data-img="${item.linkId}" role="popover">
					<a href="${uri}">${title}</a>
				</h3>

				<c:if test="${debug}">Total snippet score: <c:out value="${snippet.totalScore}"/></c:if>

				<div class="art-news-sublink">
					<c:set var="sublink" value="${iz:trunc_words(iz:clear_url(uri),'/', 60)}"/>
					<c:set var="sublink" value="${iz:mark_part_words(sublink, query)}"/>
					<c:set var="date" value="${item.date}"/>
					<span class="art-news-link-decor">${sublink}</span>
				</div>
				<p class="art-news-descr">
					<c:out value="${desc}" escapeXml="false"/>
				</p>
				<c:if test="${fn:length(snippet.subtitles) > 0}">
					<div class="ptitles secFooter">
						<c:forEach var="subtitle" items="${snippet.subtitles}" varStatus='secStatus'>
							<c:set var="subtitle_formated" value="<b>${iz:html_quotes(subtitle.title)}</b><br/>${iz:html_quotes(subtitle.description)}"/>
							<span>
								<a data-toggle="sub-popup" role="popover" data-title="${subtitle_formated}" href="${subtitle.url}" data-img="${subtitle.imageId}">
									<c:out value="${iz:mark_words(subtitle.cutedTitle,query)}" escapeXml="false"/>
								</a>
								<c:if test="${!secStatus.last}"><span class="grvline">|</span></c:if>
							</span>
						</c:forEach>
					</div>
				</c:if>
			</article>
		</c:if>
		<c:if test="${newstatus.count == 5}">
			<div id="mapblock" class="block"></div>
			<div id="bangblock" class="block"></div>

			<!--<ul class="owls-wrapper ow-01"></ul>-->
		</c:if>
	</c:forEach>
</div>
<c:if test="${fn:length(top_news) > 0}">
	<jsp:include page="/WEB-INF/tiles/mobile/google/gbtext1.jsp"/>
</c:if>
