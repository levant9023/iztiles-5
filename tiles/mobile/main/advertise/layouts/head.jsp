<%-- 
    Document   : header
    Created on : Oct 1, 2014, 11:15:04 PM
    Author     : efanchik
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<c:set var="meta_title" value="${requestScope.meta_title}" />

<head>
  <meta charset="UTF-8">
  <title>iZSearch search engine | Ads</title>
  <link rel="shortcut icon" type="image/png" href="${baseURL}/resources/img/favicon.png">
  <link rel="stylesheet" type="text/css" href="${baseURL}/resources/js/production/bootstrap/css/bootstrap.css"/>
  <link type="text/css" rel="stylesheet" href="${baseURL}/resources/css/mobile/avertise/login.css" />
  <link type="text/css" rel="stylesheet" href="${baseURL}/resources/css/mobile/avertise/api.css"/>

  <script type="text/javascript" src="${baseURL}/resources/js/production/bootstrap/js/jquery-1.11.1.min.js"></script>
  <script type="text/javascript" src="${baseURL}/resources/js/production/bootstrap/js/bootstrap.js"></script>
  <script type="text/javascript" src="${baseURL}/resources/js/production/bootstrap/js/can.min.js"></script>
  <script type="text/javascript" src="${baseURL}/resources/js/production/mvc/classes.js"></script>
  <script type="text/javascript" src="${baseURL}/resources/js/production/mvc/controls/advertise/adv_user_cart.js"></script>
</head>