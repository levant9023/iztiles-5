<footer class="bottom-fix">
  <div class="row">
    <div class="col-md-12 text-center">
      <span>&copy;iZSearch. Search it easy!</span>
    </div>
  </div>
  <!-- Google Analytics -->
  <script type="text/javascript">
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    ga('create', 'UA-58665066-1', 'auto');
    ga('send', 'pageview');
    setTimeout("ga('send', 'event', 'Pageview', 'Page visit 15 sec. or more')", 1500);
  </script>

</footer>