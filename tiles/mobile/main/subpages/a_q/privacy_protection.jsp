<%--
    Document   : more_features
    Created on : Oct 1, 2014, 11:15:04 PM
    Author     : efanchik
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="t" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="s" %>

<c:set var='title' value='${requestScope.title}' scope="request"/>

<div class="container-fluid">
  <div id="board" class="row">
    <div id="top-nav" class="row">
      <div class="col-md-4 text-left"></div>
      <div class="col-md-4 text-center"></div>
      <div class="col-md-4 text-right"></div>
    </div>
    <div id="logo" class="row">
      <div class="col-md-4 text-left"></div>
      <div class="col-md-4 text-center">
        <img src="${baseURL}/resources/img/logo_search.svg" alt="logo"/>
      </div>
      <div class="col-md-4 text-right"></div>
    </div>
    <div id="motto" class="row">
      <div class="col-md-4 text-left"></div>
      <div class="col-md-4 text-center">
        <h2>Questions</h2>
      </div>
      <div class="col-md-4 text-right"></div>
    </div>
  </div>
  <div id="arts" class="row">
    <div class="col-md-3"></div>
    <div class="col-md-6 center">
      <h1 class="grob">Does iZSearch add privacy protection to other sites I visit?</h1>
      <h3>Does iZSearch add privacy protection to other sites I visit?</h3>
      <p>No, iZSearch does not add privacy protection to websites you visit. The websites you visit are
        third-party websites and are not part of iZSearch.
        iZSearch protects your privacy as you search for information. No one can see you searching or what you
        have searched in the past.</p>
      <p>However, once you find your result in iZSearch and click on it, you are no longer actively on our
        website and our privacy protections do not apply. By leaving iZSearch and going to a different website,
        you allow that website to see your IP address and place tracking cookies on your browser. They are able
        to determine your location and possibly gather additional personally identifiable information about
        you.</p>
    </div>
    <div class="col-md-3"></div>
  </div>

</div>
