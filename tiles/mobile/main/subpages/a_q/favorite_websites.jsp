<%--
    Document   : more_features
    Created on : Oct 1, 2014, 11:15:04 PM
    Author     : efanchik
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="t" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="s" %>

<c:set var='title' value='${requestScope.title}' scope="request"/>

<div class="container-fluid">
  <div id="board" class="row">
    <div id="top-nav" class="row">
      <div class="col-md-4 text-left"></div>
      <div class="col-md-4 text-center"></div>
      <div class="col-md-4 text-right"></div>
    </div>
    <div id="logo" class="row">
      <div class="col-md-4 text-left"></div>
      <div class="col-md-4 text-center">
        <img src="${baseURL}/resources/img/logo_search.svg" alt="logo"/>
      </div>
      <div class="col-md-4 text-right"></div>
    </div>
    <div id="motto" class="row">
      <div class="col-md-4 text-left"></div>
      <div class="col-md-4 text-center">
        <h2>Questions</h2>
      </div>
      <div class="col-md-4 text-right"></div>
    </div>
  </div>
  <div id="arts" class="row">
    <div class="col-md-3"></div>
    <div class="col-md-6 center">
      <h1 class="grob">Can iZSearch store my favorite websites and bookmarks?</h1>
      <h3>Can iZSearch store my favorite websites and bookmarks?</h3>
      <p>iZSearch makes a point of not storing your searches and not recording the websites you visit.</p>
      <p>However  iZSearch has a big library of popular resources that you can directly serach on (feature similar to the sites like Pinterest and Linklist).</p>
      <p>Another (actually a standard) way to store and remember your favorite websites is through your web browser (Internet Explorer, Firefox, Safari, Chrome, Opera, etc.) The feature you want is usually called "Favorites" or "Bookmarks." You can review your browser documentation for more details on how to use this feature.</p>
    </div>
    <div class="col-md-3"></div>
  </div>

</div>
