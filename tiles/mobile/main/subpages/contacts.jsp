<%--
    Document   : more_features
    Created on : Oct 1, 2014, 11:15:04 PM
    Author     : efanchik
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="t" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="s" %>

<c:set var='title' value='more features' scope="request"/>

<div class="container-fluid">
  <div id="board" class="row">
     <div id="logo" class="row">
      <div class="text-center">
        <a href="${baseURL}"><img src="${baseURL}/resources/img/logo_search.svg" /></a>
      </div>
    </div>
    <div id="motto" class="row">
      <div class="text-center">
        <h2>Contacts</h2>
      </div>
    </div>
  </div>
  <div id="arts" class="row">
    <table id="contacts">
      <thead>
      <tr>
        <td id="ttitle" colspan="2"><h2>Our Address</h2></td>
      </tr>
      </thead>
      <tbody>
      <tr>
        <td class="key">Company name:</td>
        <td class="value">izsearch.com</td>
      </tr>
      <tr>
        <td class="key">Address :</td>
        <td class="value">1921 Palomar Oaks Way, Suite 300</td>
      </tr>
      <tr>
        <td class="key">City:</td>
        <td class="value">Carlsbad</td>
      </tr>
      <tr>
        <td class="key">Country:</td>
        <td class="value">US</td>
      </tr>
      <tr>
        <td class="key">State:</td>
        <td class="value">California</td>
      </tr>
      <tr>
        <td class="key">Zip:</td>
        <td class="value">CA 92008</td>
      </tr>
      <tr>
        <td class="key">Phone:</td>
        <td class="value">(858) 480-9531</td>
      </tr>
      </tbody>
    </table>
  </div>

</div>